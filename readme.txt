186.186 Fractals

Implementation:
- Fractal Terrains
- Julia set via Mandelbrot set


186.166 Design and Implementation of a rendering engine

Implementation:
- Fractal Terrains
- View Frustum Culling
- Quadtree


Projektstruktur:
- doc
	Documentation
- literature
	Some useful documents
- src
	C# Visual Studio 2015 Project
- submission
	Executables of Fractal Terrains and Julia set via Mandelbrot set(Release-Version)