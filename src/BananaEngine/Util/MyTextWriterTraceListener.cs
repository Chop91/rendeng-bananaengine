﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace BananaEngine.Util
{
    public class MyTextWriterTraceListener : TextWriterTraceListener
    {
        /// <summary>
        /// Constructor of the TextWriterTraceListener.
        /// </summary>
        public MyTextWriterTraceListener()
            : base($"{ConfigurationManager.AppSettings["windowName"]}_{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.log")
        {
            Filter = new EventTypeFilter(SourceLevels.Information);
        }

        /// <summary>
        /// Write a custom lablel.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public override void Write(string message)
        {
            if (message != null)
            {
                var messageParts = message.Split(' ').ToList();
                var type = messageParts.ElementAtOrDefault(1);
                var label = type == null ? $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}: " : $"{DateTime.Now:yyyy-MM-dd HH:mm:ss},{type} ";
                Writer.Write(label);
            }
        }
    }
}