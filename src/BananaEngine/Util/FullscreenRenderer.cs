﻿using BananaEngine.Geometry.Models.Primitives._2D;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Util
{
    public class FullscreenRenderer
    {
        private readonly Quad2D _fullscreeQuad;
        private readonly Shader _fullscreenShader;

        private Matrix4 _modelMatrix;
        private readonly Matrix4 _scale;

        private float _heightcrop;

        /// <summary>
        /// Constructor of the fullscreen renderer.
        /// </summary>
        public FullscreenRenderer()
        {
            _fullscreeQuad = new Quad2D();
            _fullscreenShader = ShaderManager.GetShader("./Shader/fullscreen.vert", "./Shader/fullscreen.frag");

            _modelMatrix = Matrix4.Identity;
            _scale = Matrix4.CreateScale(0.5f);
            _heightcrop = 1.0f;
        }

        // Renders images in coordinate system (bottom left system)
        // images have their center at their bottom left corner too
        // width is the width of the image of the viewspace between 0 and 1
        // height scales according to width, or is explicitly scalable
        // via heightcrop. CAVEAT: Using height-crop will make
        // vertical alignment trial&error!
        public void RenderAt(uint textureHandle, float x, float y, float width, float heightcrop = 1.0f)
        {
            _modelMatrix = Matrix4.CreateTranslation(-1f / width + 1 + 2 / width * x, -1f / width + 1 + 2 / width * y, 0.0f) * Matrix4.CreateScale(width);
            this._heightcrop = heightcrop;
            Render(textureHandle);
        }

        /// <summary>
        /// Renders the provided texture fullscreen.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        public void RenderFullscreen(uint textureHandle)
        {
            _modelMatrix = Matrix4.Identity;
            Render(textureHandle);
        }


        /// <summary>
        /// Renders the provided texture top left.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        public void RenderTopLeft(uint textureHandle)
        {
            _modelMatrix = Matrix4.CreateTranslation(-1.0f, 1.0f, 0.0f) * _scale;
            Render(textureHandle);
        }

        /// <summary>
        /// Renders the provided texture top right.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        public void RenderTopRight(uint textureHandle)
        {
            _modelMatrix = Matrix4.CreateTranslation(1.0f, 1.0f, 0.0f) * _scale;
            Render(textureHandle);
        }

        /// <summary>
        /// Renders the provided texture bottom left.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        public void RenderBottomLeft(uint textureHandle)
        {
            _modelMatrix = Matrix4.CreateTranslation(-1.0f, -1.0f, 0.0f) * _scale;
            Render(textureHandle);
        }

        /// <summary>
        /// Renders the provided texture bottom right.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        public void RenderBottomRight(uint textureHandle)
        {
            _modelMatrix = Matrix4.CreateTranslation(1.0f, -1.0f, 0.0f) * _scale;
            Render(textureHandle);
        }

        /// <summary>
        /// Clears the buffer.
        /// </summary>
        public void Clear()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        /// <summary>
        /// Deletes the fullscreen quad.
        /// </summary>
        public void Delete()
        {
            _fullscreeQuad.Delete();
        }

        /// <summary>
        /// Renders the provided texture.
        /// </summary>
        /// <param name="textureHandle">The id of the texture.</param>
        private void Render(uint textureHandle)
        {
            _fullscreenShader.UseShader();

            GL.BindVertexArray(_fullscreeQuad.Mesh2D.Vao);

            GL.UniformMatrix4(1, false, ref _modelMatrix);
            GL.Uniform1(3, _heightcrop);
            GL.Uniform1(2, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, textureHandle);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.BindVertexArray(0);
        }
    }
}