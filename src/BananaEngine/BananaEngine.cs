﻿using AntTweakBar;
using System;
using System.Diagnostics;
using BananaEngine.Graphics;

namespace BananaEngine
{
    internal class BananaEngine
    {
        // Catching exceptions according to: https://github.com/TomCrypto/AntTweakBar.NET/blob/master/Samples/OpenTK-OpenGL/Program.cs

        public const int ExitSuccess = 0;
        public const int ExitFailure = 1;

        [STAThread]
        public static int Main()
        {
            try
            {
                using (var renderer = new Renderer()) // TODO: basic default renderer
                {
                    renderer.Run(60, 60);
                }

                return ExitSuccess;
            }
            catch (AntTweakBarException e)
            {
                Trace.TraceError("AntTweakBar error: " + e.Message
                    + "Exception details: " + e.Details
                    + "\n======= STACK TRACE =======\n"
                    + e.StackTrace);

                return ExitFailure;
            }
            catch (Exception e)
            {
                Trace.TraceError("An error occurred: " + e.Message
                    + "\n======= STACK TRACE =======\n"
                    + e.StackTrace);

                return ExitFailure;
            }
        }
    }
}