#version 430 core

layout(location = 1) in vec4 v_position;
layout(location = 2) in vec3 v_normal;
layout(location = 3) in vec2 v_uv;

out vec2 f_uv;
out vec3 f_object_normal;
out vec3 f_object_to_camera_direction;
out vec3 f_object_to_light_direction;
out float f_attenuation;


struct Light {
	vec3 world_light_position;
	vec3 light_color_ambient;
	vec3 light_color_diffuse;
	vec3 light_color_specular;
	float attenuation_constant;
	float attenuation_linear;
	float attenuation_quadratic;
	float shininess;
};

layout(location = 1) uniform mat4 u_M;
layout(location = 2) uniform mat4 u_V;
layout(location = 3) uniform mat4 u_VP;
layout(location = 6) uniform vec3 u_camera_position;
layout(location = 7) uniform Light u_light;


void main()
{
	// Positions
	gl_Position = u_VP * u_M * v_position;

	//UVs 
	f_uv = v_uv;

	// Normals
	f_object_normal = (u_M * vec4(v_normal, 0.0)).xyz;
	
	
	// Direction from the point on the surface to the camera
	vec3 object_position = (u_M * v_position).xyz;
	f_object_to_camera_direction = u_camera_position - object_position;

	// Direction from the point on the surface to the light
	f_object_to_light_direction = u_light.world_light_position.xyz - object_position;

	// Distance from the light source to the point on the surface
	float light_to_object_distance = distance(u_light.world_light_position, object_position);

	// Calculate the light attenuation
	f_attenuation = 1.0; // directional light has no attenuation
	// / (u_light.attenuation_constant + u_light.attenuation_linear * light_to_object_distance + u_light.attenuation_quadratic * pow(light_to_object_distance, 2.0));
}