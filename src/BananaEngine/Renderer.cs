﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using ClearBufferMask = OpenTK.Graphics.OpenGL4.ClearBufferMask;
using EnableCap = OpenTK.Graphics.OpenGL4.EnableCap;
using GL = OpenTK.Graphics.OpenGL4.GL;

namespace BananaEngine
{
    internal class Renderer : GameWindow
    {
        // Constants
        private const int NumberOfKeysPressedSimultaneously = 3;

        private float _deltaTime;
        private List<Key> _keyList;
        private bool _lockCursor;


        public Renderer()
            : base(800, 600, new GraphicsMode(32, 24, 0, 4))
        {
            // Get data from App.config
            var windowWidth = Convert.ToInt32(ConfigurationManager.AppSettings["windowWidth"]);
            var windowHeight = Convert.ToInt32(ConfigurationManager.AppSettings["windowHeight"]);
            var fullscreen = Convert.ToBoolean(ConfigurationManager.AppSettings["fullscreen"]);
            var renderFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["renderFrequency"]);
            var updateFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["updateFrequency"]);
            var iconPath = Convert.ToString(ConfigurationManager.AppSettings["iconPath"]);
            var windowName = Convert.ToString(ConfigurationManager.AppSettings["windowName"]);

            ClientSize = new Size(windowWidth, windowHeight);
            Title = windowName;
            Icon = new Icon(iconPath);

            if (fullscreen)
            {
                WindowState = WindowState.Fullscreen;
                TargetRenderFrequency = renderFrequency;
                TargetUpdateFrequency = updateFrequency;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.ClearColor(0, 0, 0, 0);
            GL.Enable(EnableCap.DepthTest);
            WindowBorder = WindowBorder.Fixed;

            _deltaTime = 0.0f;
            _keyList = new List<Key>();
            CursorVisible = false;
            _lockCursor = true;
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            // TODO: Update scene here
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            SwapBuffers();
        }

        public override void Exit()
        {
            // TODO: Exit and delete scene here.
            base.Exit();
        }


        #region Minor event handling

        // According to suggestion from here to handle multiple keys at once: http://neokabuto.blogspot.co.at/2014/01/opentk-tutorial-5-basic-camera.html
        private void MoveCamera(float deltaTime)
        {
            foreach (var key in _keyList)
            {
                switch (key)
                {
                    case Key.W:
                        
                        break;
                    case Key.S:
                        
                        break;
                    case Key.A:
                        
                        break;
                    case Key.D:
                        
                        break;
                    case Key.R:
                        
                        break;
                    case Key.F:
                        
                        break;
                    case Key.Up:
                        
                        break;
                    case Key.Down:
                        
                        break;
                    case Key.Left:
                        
                        break;
                    case Key.Right:
                        
                        break;
                }
            }

            //keyList.Clear();
        }

        private Vector2 _lastMousePosition = new Vector2();

        private void ResetCursor()
        {
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
            _lastMousePosition = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            if (Focused && _lockCursor)
            {
                ResetCursor();
            }
        }

        

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            GL.Viewport(this.ClientRectangle);
        }

        protected override void Dispose(bool manual)
        {
            base.Dispose(manual);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            base.OnKeyUp(e);
            _keyList.RemoveAll(x => x == e.Key);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
        }

        #endregion
    }
}