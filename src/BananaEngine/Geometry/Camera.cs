﻿using System;
using System.Drawing;
using OpenTK;

namespace BananaEngine.Geometry
{
    public class Camera
    {
        private const float MovementSpeed = 100.0f;
        private const float CameraSensitivity = 0.5f;
        
        
        public Vector3 UpVector { get; set; }

        public Vector3 Position { get; private set; }
        public Matrix4 Rotation { get; private set; }
        public Vector3 ViewDirection { get; set; }
        public float Fov { get; set; }
        public float FarPlane { get; set; }
        public float NearPlane { get; set; }
        public float Ratio { get; set; }

        public Matrix4 InverseViewMatrix;
        public Matrix4 ViewMatrix;
        public Matrix4 ProjectionMatrix;
        public Matrix4 ViewProjectionMatrix;

        /// <summary>
        /// Constructor of the camera.
        /// </summary>
        /// <param name="fov">The field of view of the camera.</param>
        /// <param name="nearPlane">The near plane of the camera.</param>
        /// <param name="farPlane">The far plane of the camera.</param>
        /// <param name="size">The size of the window.</param>
        public Camera(float fov, float nearPlane, float farPlane, Size size)
        {
            // Default view direction and up vector
            ViewDirection = -Vector3.UnitZ;
            UpVector = Vector3.UnitY;

            Fov = fov;
            FarPlane = farPlane;
            NearPlane = nearPlane;
            Ratio = size.Width / (float) size.Height;
        }

        /// <summary>
        /// Generates the view, projection and view projection matrix. The view matrix is calculated by inverting the provided inverse view matrix.
        /// So the debug camera can follow the main camera.
        /// </summary>
        /// <param name="inverseViewMatrix">The provided inverse view matrix.</param>
        public void GenerateModelAndViewAndProjection(Matrix4 inverseViewMatrix)
        {
            InverseViewMatrix = inverseViewMatrix;
            ViewMatrix = Matrix4.Invert(inverseViewMatrix);
            ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(Fov * ((float)Math.PI / 180.0f), Ratio, NearPlane, FarPlane);
            ViewProjectionMatrix = ViewMatrix * ProjectionMatrix;
        }

        /// <summary>
        /// Generates the view, projection and view projection matrix.
        /// </summary>
        public void GenerateModelAndViewAndProjection()
        {
            ViewMatrix = Matrix4.LookAt(Position, Position + ViewDirection, UpVector);
            ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(Fov * ((float)Math.PI / 180.0f), Ratio, NearPlane, FarPlane);
            ViewProjectionMatrix = ViewMatrix * ProjectionMatrix;
            InverseViewMatrix = Matrix4.Invert(ViewMatrix);
        }

        /// <summary>
        /// Updates the view direction.
        /// </summary>
        /// <param name="mouseDelta">The delta the mouse moved.</param>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void UpdateViewDirection(Vector2 mouseDelta, float deltaTime)
        {
            var rotationAxis = Vector3.Cross(ViewDirection, UpVector);
            var rotationX = -mouseDelta.X * CameraSensitivity * deltaTime;
            var rotationY = -mouseDelta.Y * CameraSensitivity * deltaTime;
            rotationY = Math.Min(90.0f, Math.Max(rotationY, -90.0f));

            Rotation = Matrix4.CreateFromAxisAngle(UpVector, rotationX) *
                          Matrix4.CreateFromAxisAngle(rotationAxis, rotationY);
            ViewDirection = Vector3.TransformPerspective(ViewDirection, Rotation);
        }

        /// <summary>
        /// Moves the camera forward.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void MoveForward(float deltaTime)
        {
            Position += MovementSpeed * ViewDirection * deltaTime;
        }

        /// <summary>
        /// Moves the camera backward.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void MoveBackward(float deltaTime)
        {
            Position += -MovementSpeed * ViewDirection * deltaTime;
        }

        /// <summary>
        /// Strafes the camera left.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void StrafeLeft(float deltaTime)
        {
            var strafeDirection = Vector3.Cross(ViewDirection, UpVector);
            Position += -MovementSpeed * strafeDirection * deltaTime;
        }

        /// <summary>
        /// Strafes the camera right.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void StrafeRight(float deltaTime)
        {
            var strafeDirection = Vector3.Cross(ViewDirection, UpVector);
            Position += MovementSpeed * strafeDirection * deltaTime;
        }

        /// <summary>
        /// Moves the camera up.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void MoveUp(float deltaTime)
        {
            Position += MovementSpeed * UpVector * deltaTime;
        }

        /// <summary>
        /// Moves the camera down.
        /// </summary>
        /// <param name="deltaTime">The delta time elapsed.</param>
        public void MoveDown(float deltaTime)
        {
            Position += -MovementSpeed * UpVector * deltaTime;
        }
    }
}