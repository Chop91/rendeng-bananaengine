﻿using OpenTK;
using OpenTK.Graphics;
namespace BananaEngine.Geometry
{
    public class Light
    {
        public enum LightSource
        {
            Point,
            Sun,
            Spot,
            Hemi,
            Area
        }

        internal Matrix4 ModelMatrix;

        public bool CastShadow { get; set; }

        public string Name { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 WorldPosition { get; set; }
        public Vector3 LightDirection { get; set; }
        public Vector3 Direction { get; set; }
        public Color4 ColorAmbient { get; set; }
        public Color4 ColorDiffuse { get; set; }
        public Color4 ColorSpecular { get; set; }
        public float AttentuationQuadratic { get; set; }
        public float AttentuationLinear { get; set; }
        public float AttentuationConstant { get; set; }
        public float AngleInnerCone { get; set; }
        public float AngleOuterCone { get; set; }

        public LightSource LightSourceType { get; set; }


        /// <summary>
        /// Constructor of the light.
        /// </summary>
        public Light()
        {
            // Default settings
            AngleInnerCone = 0.0f;
            AngleOuterCone = 0.0f;
            AttentuationConstant = 1000.0f;
            AttentuationLinear = 1000.0f;
            AttentuationQuadratic = 1000.0f;
            ColorAmbient = new Color4(0.25f, 0.25f, 0.25f, 1.0f);
            ColorDiffuse = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            ColorSpecular = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            Position = new Vector3(100, 100, 100);
            LightDirection = new Vector3(0.0f, 0.0f, 0.75f);
            WorldPosition = Position;
            Name = "directional_light";
            ModelMatrix = Matrix4.Identity;
            CastShadow = false;
        }
    }
}