﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace BananaEngine.Geometry.Models.Primitives._3D
{
    public class Cube : Primitive3D
    {
        /// <summary>
        /// Constructor of the cube.
        /// </summary>
        public Cube()
        {
            Setup();
        }

        /// <summary>
        /// Sets the mesh of the cube up.
        /// </summary>
        protected override void Setup()
        {
            var positions = new List<Vector4>
            {
                new Vector4(-1.0f, -1.0f, -1.0f, 1.0f),
                new Vector4(1.0f, -1.0f, -1.0f, 1.0f),
                new Vector4(1.0f, 1.0f, -1.0f, 1.0f),
                new Vector4(-1.0f, 1.0f, -1.0f, 1.0f),
                new Vector4(-1.0f, -1.0f, 1.0f, 1.0f),
                new Vector4(1.0f, -1.0f, 1.0f, 1.0f),
                new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                new Vector4(-1.0f, 1.0f, 1.0f, 1.0f)
            };

            var indices = new List<int>
            {
                // quad indices
                // back
                0, 3, 2, 1,

                // top
                2, 3, 7, 6,

                // left
                0, 4, 7, 3,

                // right
                1, 2, 6, 5,

                // front
                4, 5, 6, 7,

                // bottom
                0, 1, 5, 4
            };

            var vertices = positions.Select(p => new Mesh.Vertex { Position = p, Color = Vector3.UnitY }).ToList();
            Mesh = new Mesh(vertices, indices);
        }
    }
}