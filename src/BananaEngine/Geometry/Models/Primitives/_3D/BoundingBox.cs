﻿using System;
using System.Collections.Generic;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Geometry.Models.Primitives._3D
{
    public class BoundingBox : Cube
    {
        /// <summary>
        /// Constructor of the bounding box.
        /// </summary>
        /// <param name="vertices">The vertices to calculate a bounding box for.</param>
        public BoundingBox(List<Mesh.Vertex> vertices)
        {
            Shader = ShaderManager.GetShader("./Shader/solid.vert", "./Shader/solid.frag");
            CalculateBoundingBox(vertices);
        }

        /// <summary>
        /// Calculates a bounding box for the given vertices.
        /// </summary>
        /// <param name="vertices">The vertices to calculate a bounding box for.</param>
        private void CalculateBoundingBox(List<Mesh.Vertex> vertices)
        {
            var minX = float.MaxValue;
            var maxX = float.MinValue;
            var minY = float.MaxValue;
            var maxY = float.MinValue;
            var minZ = float.MaxValue;
            var maxZ = float.MinValue;

            foreach (var vertex in vertices)
            {
                var position = vertex.Position;
                minX = Math.Min(minX, position.X);
                maxX = Math.Max(maxX, position.X);
                minY = Math.Min(minY, position.Y);
                maxY = Math.Max(maxY, position.Y);
                minZ = Math.Min(minZ, position.Z);
                maxZ = Math.Max(maxZ, position.Z);
            }

            var boundingBoxVertices = new List<Mesh.Vertex>(8);
            var v = new Mesh.Vertex
            {
                Position = new Vector4(minX, minY, minZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(minX, minY, maxZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(minX, maxY, minZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(minX, maxY, maxZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(maxX, minY, minZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(maxX, minY, maxZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(maxX, maxY, minZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            v = new Mesh.Vertex
            {
                Position = new Vector4(maxX, maxY, maxZ, 1),
                Color = Vector3.One
            };
            boundingBoxVertices.Add(v);

            var indices = new List<int>
            {
                // quad indices
                // back
                0, 2, 6, 4,

                // top
                3, 2, 6, 7,

                // left
                0, 2, 3, 1,

                // right
                4, 6, 7, 5,

                // front
                1, 3, 7, 5,

                // bottom
                1, 0, 4, 5
            };

            Mesh = new Mesh(boundingBoxVertices, indices);
        }

        /// <summary>
        /// Renders the bounding box.
        /// </summary>
        /// <param name="camera">The camera.</param>
        /// <param name="transformationMatrix">The transformation matrix calculated according to the user input.</param>
        public void Render(Camera camera, Matrix4 transformationMatrix)
        {
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

            Shader.UseShader();
            
            // Bind matrices
            GL.BindVertexArray(Mesh.Vao);
            GL.UniformMatrix4(1, false, ref TransformedModelMatrix);
            GL.UniformMatrix4(2, false, ref camera.ViewProjectionMatrix);

            // Draw mesh
            GL.DrawElements(PrimitiveType.Quads, Mesh.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }
    }
}