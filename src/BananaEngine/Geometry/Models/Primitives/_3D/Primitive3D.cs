﻿namespace BananaEngine.Geometry.Models.Primitives._3D
{
    /// <summary>
    /// Abstract class to group three dimensional primitives.
    /// </summary>
    public abstract class Primitive3D : Model
    {
        protected abstract void Setup();
    }
}