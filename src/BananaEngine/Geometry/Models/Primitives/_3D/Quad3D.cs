﻿using System.Collections.Generic;
using OpenTK;

namespace BananaEngine.Geometry.Models.Primitives._3D
{
    public class Quad3D : Primitive3D
    {
        /// <summary>
        /// Constructor of the 3D quad.
        /// </summary>
        public Quad3D()
        {
            Setup();
        }

        /// <summary>
        /// Sets the mesh of the 3D quad up.
        /// </summary>
        protected override void Setup()
        {
            var vertexUpLeft = new Mesh.Vertex();
            var vertexUpRight = new Mesh.Vertex();
            var vertexDownRight = new Mesh.Vertex();
            var vertexDownLeft = new Mesh.Vertex();

            vertexUpLeft.Position = new Vector4(-1.0f, 0.0f, -1.0f, 1.0f);
            vertexUpRight.Position = new Vector4(1.0f, 0.0f, -1.0f, 1.0f);
            vertexDownRight.Position = new Vector4(1.0f, 0.0f, 1.0f, 1.0f);
            vertexDownLeft.Position = new Vector4(-1.0f, 0.0f, 1.0f, 1.0f);

            vertexUpLeft.Normal = Vector3.Normalize(Vector3.Cross(
                vertexUpRight.Position.Xyz - vertexUpLeft.Position.Xyz,
                vertexDownLeft.Position.Xyz - vertexUpLeft.Position.Xyz));
            vertexUpRight.Normal = Vector3.Normalize(Vector3.Cross(
                vertexUpLeft.Position.Xyz - vertexUpRight.Position.Xyz,
                vertexDownRight.Position.Xyz - vertexUpRight.Position.Xyz));
            vertexDownRight.Normal = Vector3.Normalize(Vector3.Cross(
                vertexUpRight.Position.Xyz - vertexDownRight.Position.Xyz,
                vertexDownLeft.Position.Xyz - vertexDownRight.Position.Xyz));
            vertexDownLeft.Normal = Vector3.Normalize(Vector3.Cross(
                vertexUpLeft.Position.Xyz - vertexDownLeft.Position.Xyz,
                vertexDownRight.Position.Xyz - vertexDownLeft.Position.Xyz));

            vertexUpLeft.Uv = new Vector2(0.0f, 0.0f);
            vertexUpRight.Uv = new Vector2(1.0f, 0.0f);
            vertexDownRight.Uv = new Vector2(1.0f, 1.0f);
            vertexDownLeft.Uv = new Vector2(0.0f, 1.0f);

            var vertices = new List<Mesh.Vertex>
            {
                vertexUpLeft,
                vertexDownLeft,
                vertexUpRight,
                vertexDownRight,
                vertexUpRight,
                vertexDownLeft
            };

            var indices = new List<int>
            {
                // triangle indices
                0, 1, 2,
                3, 2, 1
            };

            Mesh = new Mesh(vertices, indices);
        }
    }
}