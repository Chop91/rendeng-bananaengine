﻿using BananaEngine.Graphics;
using OpenTK;

namespace BananaEngine.Geometry.Models.Primitives._3D
{
    public class Model
    {
        public Mesh Mesh { get; protected set; }
        protected Shader Shader; 

        public Matrix4 ModelMatrix;
        public Matrix4 TransformedModelMatrix;

        /// <summary>
        /// Constructor of the model.
        /// </summary>
        public Model()
        {
            ModelMatrix = Matrix4.Identity;
            TransformedModelMatrix = Matrix4.Identity;
        }

        /// <summary>
        /// Deletes the mesh of the model.
        /// </summary>
        public void Delete()
        {
            Mesh.Delete();
        }
    }
}