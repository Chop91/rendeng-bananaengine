﻿using System.Collections.Generic;
using OpenTK;

namespace BananaEngine.Geometry.Models.Primitives._2D
{
    public sealed class Quad2D : Primitive2D
    {
        /// <summary>
        /// Constructor of the 2D quad.
        /// </summary>
        public Quad2D()
        {
            Setup();
        }

        /// <summary>
        /// Sets the mesh of the 2D quad up.
        /// </summary>
        protected override void Setup()
        {
            Mesh2D.Vertex vertexUpLeft;
            Mesh2D.Vertex vertexUpRight;
            Mesh2D.Vertex vertexDownRight;
            Mesh2D.Vertex vertexDownLeft;

            vertexUpLeft.Position = new Vector2(-1.0f, 1.0f);
            vertexUpRight.Position = new Vector2(1.0f, 1.0f);
            vertexDownRight.Position = new Vector2(1.0f, -1.0f);
            vertexDownLeft.Position = new Vector2(-1.0f, -1.0f);

            vertexUpLeft.Uv = new Vector2(0.0f, 1.0f);
            vertexUpRight.Uv = new Vector2(1.0f, 1.0f);
            vertexDownRight.Uv = new Vector2(1.0f, 0.0f);
            vertexDownLeft.Uv = new Vector2(0.0f, 0.0f);

            var vertices = new List<Mesh2D.Vertex>
            {
                vertexUpLeft,
                vertexDownLeft,
                vertexUpRight,
                vertexDownRight,
                vertexUpRight,
                vertexDownLeft
            };

            var indices = new List<int>
            {
                // triangle indices
                0, 1, 2,
                3, 2, 1
            };

            Mesh2D = new Mesh2D(vertices, indices);
        }
    }
}