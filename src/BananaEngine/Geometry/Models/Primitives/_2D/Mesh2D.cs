﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Geometry.Models.Primitives._2D
{
    public class Mesh2D
    {
        public int Vao;
        internal int Vbo;
        internal int Ebo;

        public List<Vertex> Vertices { get; set; }
        public List<int> Indices { get; set; }

        public Texture TextureDiffuse { get; set; }

        /// <summary>
        /// Constructor of the two dimensional mesh.
        /// </summary>
        /// <param name="vertices">The vertices of the mesh.</param>
        /// <param name="indices">The indices of the mesh.</param>
        public Mesh2D(List<Vertex> vertices, List<int> indices)
        {
            Vertices = vertices;
            Indices = indices;
            Setup();
        }

        /// <summary>
        /// Deletes all the opengl related stuff of the two dimensional mesh.
        /// </summary>
        public void Delete()
        {
            GL.DeleteBuffer(Ebo);
            GL.DeleteBuffer(Vbo);
            GL.DeleteVertexArray(Vao);
        }

        /// <summary>
        /// Sets the two dimensional mesh up.
        /// </summary>
        private void Setup()
        {
            // Generate buffers
            GL.GenVertexArrays(1, out Vao);
            GL.GenBuffers(1, out Vbo);
            GL.GenBuffers(1, out Ebo);

            // Bind everything
            GL.BindVertexArray(Vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Vbo);
            GL.BufferData(BufferTarget.ArrayBuffer,
                (IntPtr)(Vertices.Count * Marshal.SizeOf(typeof(Vertex))), Vertices.ToArray(),
                BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, Ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(Indices.Count * sizeof(int)),
                Indices.ToArray(), BufferUsageHint.StaticDraw);

            // Vertex positions
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                IntPtr.Zero);

            // Vertex uv-coordinates
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                Marshal.OffsetOf(typeof(Vertex), "Uv"));

            // Unbind everything
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        /// <summary>
        /// This struct represents a vertex in 2D.
        /// </summary>
        public struct Vertex
        {
            public Vector2 Position;
            public Vector2 Uv;
        }
    }
}