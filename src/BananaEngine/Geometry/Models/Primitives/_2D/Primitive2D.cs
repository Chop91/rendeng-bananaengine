﻿namespace BananaEngine.Geometry.Models.Primitives._2D
{
    /// <summary>
    /// Abstract class to group two dimensional primitives.
    /// </summary>
    public abstract class Primitive2D
    {
        public Mesh2D Mesh2D { get; protected set; }

        protected abstract void Setup();

        protected internal void Delete()
        {
            Mesh2D.Delete();
        }
    }
}