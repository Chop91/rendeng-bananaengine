﻿using System;
using System.Drawing;
using System.Linq;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Graphics
{
    public class CubeMapFbo
    {
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }

        private readonly int[] _fbo;
        private uint _depthBuffer;

        public int ColorTexture { get; private set; }
        
        /// <summary>
        /// Constructor of the cube map frame buffer object.
        /// </summary>
        /// <param name="windowWidth">The width of the window.</param>
        /// <param name="windowHeight">The height of the window.</param>
        public CubeMapFbo(int windowWidth, int windowHeight)
        {
            WindowWidth = windowWidth;
            WindowHeight = windowHeight;
            _fbo = new int[6];
        }

        /// <summary>
        /// Creates the frame buffer object.
        /// </summary>
        /// <param name="loadData">Loads some predefined test data.</param>
        /// <returns>True, if everything works. False, if not.</returns>
        public bool CreateFbo(bool loadData = false)
        {
            // Generate fbo, depth buffer and color texture
            GL.GenFramebuffers(6, _fbo);
            GL.GenRenderbuffers(1, out _depthBuffer);
            ColorTexture = GL.GenTexture();

            // Bind fbo and depth buffer
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _depthBuffer);

            // Bind color texture
            GL.BindTexture(TextureTarget.TextureCubeMap, ColorTexture);

            // Fixes seam-artifacts due to numerical precision limitations
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            // Filter texture
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            for (var face = 0; face < 6; face++)
            {
                // Give an empty image to OpenGL ( the last "0" )
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + face, 0, PixelInternalFormat.Rgb, WindowWidth, WindowHeight, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero);

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, _fbo[face]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.TextureCubeMapPositiveX + face, ColorTexture, 0);
            }

            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, WindowWidth, WindowHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, _depthBuffer);

            // Always check that our framebuffer is ok
            var error = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (error != FramebufferErrorCode.FramebufferComplete)
            {
                Console.Error.WriteLine(error.ToString());
                return false;
            }

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindTexture(TextureTarget.TextureCubeMap, 0);

            if (loadData)
            {
                LoadCubeMap();
            }

            return true;
        }

        /// <summary>
        /// Attaches the provided face to the frame buffer object.
        /// </summary>
        /// <param name="face">The index of the face.</param>
        public void AttachFaceToFbo(int face)
        {
            // Attach to fbo
            GL.Clear(ClearBufferMask.DepthBufferBit);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.TextureCubeMapPositiveX + face, ColorTexture, 0);
        }

        /// <summary>
        /// Binds the frame buffer object to the provided face.
        /// </summary>
        /// <param name="face">The index of the face.</param>
        public void BindFbo(int face)
        {
            // Render to framebuffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _fbo[face]);
            GL.Viewport(0, 0, WindowWidth, WindowHeight);
        }

        /// <summary>
        /// Unbinds the frame buffer object.
        /// </summary>
        public void UnbindFbo()
        {
            // Render to screen
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, WindowWidth, WindowHeight);
        }

        /// <summary>
        /// Loads an image into the cube map.
        /// </summary>
        /// <returns>The index of the color texture.</returns>
        public int LoadCubeMap()
        {
            var textureFiles = new[]
            {
                "./Data/Skybox/posx.png",
                "./Data/Skybox/negx.png",
                "./Data/Skybox/posy.png",
                "./Data/Skybox/negy.png",
                "./Data/Skybox/posz.png",
                "./Data/Skybox/negz.png",
            };

            var faces = textureFiles.Select(texture => new Bitmap(texture)).ToList();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.TextureCubeMap, ColorTexture);

            for (var i = 0; i < faces.Count; i++)
            {
                var data = faces[i].LockBits(new Rectangle(0, 0, faces[i].Width, faces[i].Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgb, data.Width, data.Height, 0, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);

                faces[i].UnlockBits(data);

            }

            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);

            GL.BindTexture(TextureTarget.TextureCubeMap, 0);

            return ColorTexture;
        }

        /// <summary>
        /// Deletes all the opengl related stuff.
        /// </summary>
        public void Delete()
        {
            GL.DeleteTexture(ColorTexture);
        }
    }
}
