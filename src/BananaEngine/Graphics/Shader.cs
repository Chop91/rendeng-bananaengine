﻿using System.Diagnostics;
using System.IO;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Graphics
{
    public class Shader
    {
        public int ProgramHandle { get; set; }
        private readonly int _vertexHandle;
        private int? _geometryHandle;
        private readonly int _fragmentHandle;

        /// <summary>
        /// Constructor of the shader.
        /// </summary>
        /// <param name="vertexShader">The path of the vertex shader.</param>
        /// <param name="fragmentShader">The path of the fragment shader.</param>
        public Shader(string vertexShader, string fragmentShader)
            : this(vertexShader, null, fragmentShader)
        {}

        /// <summary>
        /// Constructor of the shader.
        /// </summary>
        /// <param name="vertexShader">The path of the vertex shader.</param>
        /// <param name="geometryShader">The path of the geometry shader.</param>
        /// <param name="fragmentShader">The path of the fragment shader.</param>
        public Shader(string vertexShader, string geometryShader, string fragmentShader)
        {
            ProgramHandle = 0;
            Trace.TraceInformation("Creating Shaders: {0} | {1} | {2}", vertexShader, geometryShader, fragmentShader);

            // Create shader program
            ProgramHandle = GL.CreateProgram();
            if (ProgramHandle == 0)
            {
                Trace.TraceError(GL.GetProgramInfoLog(ProgramHandle));
                System.Environment.Exit(BananaEngine.ExitFailure);
            }

            // Load vertex shader
            LoadShader(vertexShader, ShaderType.VertexShader, ProgramHandle, out _vertexHandle);

            // Load geometry shader
            if (geometryShader != null)
            {
                int handle;
                LoadShader(geometryShader, ShaderType.GeometryShader, ProgramHandle, out handle);
                _geometryHandle = handle;
            }

            // Load fragment shader
            LoadShader(fragmentShader, ShaderType.FragmentShader, ProgramHandle, out _fragmentHandle);

            // Link the shaders
            Link();
        }

        /// <summary>
        /// Uses the shader program.
        /// </summary>
        public void UseShader()
        {
            GL.UseProgram(ProgramHandle);
        }

        /// <summary>
        /// Loads and compiles the shader.
        /// </summary>
        /// <param name="shader">The path of the shader.</param>
        /// <param name="shaderType">The type of the shader.</param>
        /// <param name="handle">The handle of the shader program</param>
        /// <param name="adress">The handle of shader type.</param>
        private void LoadShader(string shader, ShaderType shaderType, int handle, out int adress)
        {
            adress = GL.CreateShader(shaderType);

            using (var reader = new StreamReader(shader))
            {
                GL.ShaderSource(adress, reader.ReadToEnd());
            }

            GL.CompileShader(adress);
            GL.AttachShader(handle, adress);
            Trace.TraceInformation("Load {0}: {1}", shaderType.ToString(), GL.GetShaderInfoLog(adress));
        }

        /// <summary>
        /// Links the shader program.
        /// </summary>
        private void Link()
        {
            GL.LinkProgram(ProgramHandle);
            Trace.TraceInformation("Link: {0}", GL.GetProgramInfoLog(ProgramHandle));
        }

        /// <summary>
        /// Deletes all the opengl related stuff.
        /// </summary>
        public void Unload()
        {
            GL.DeleteProgram(ProgramHandle);
            GL.DeleteShader(_vertexHandle);
            GL.DeleteShader(_fragmentHandle);

            if (_geometryHandle.HasValue)
            {
                GL.DeleteShader(_geometryHandle.Value);
            }
        }
    }
}