﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK.Graphics.OpenGL;

namespace BananaEngine.Graphics
{
    public static class TextureManager
    {
        private static readonly Dictionary<string, Texture> KnownTextures = new Dictionary<string, Texture>();

        /// <summary>
        /// Gets a texture from a given path. If the texture is already known it simply returns it without loading it again.
        /// </summary>
        /// <param name="textureFilePath">The path of the texture.</param>
        /// <returns>The texture from the given path.</returns>
        public static Texture GetTextureFromFilePath(string textureFilePath)
        {
            return GetTextureFromFilePath(textureFilePath, TextureTarget.Texture2D);
        }

        /// <summary>
        /// Gets a texture from a given path. If the texture is already known it simply returns it without loading it again.
        /// </summary>
        /// <param name="textureFilePath">The path of the texture.</param>
        /// <returns>The texture from the given path.</returns>
        public static Texture GetTextureFromFilePath(string textureFilePath, TextureTarget textureTarget)
        {
            // If we already know of this texture, simply return it
            if (KnownTextures.ContainsKey(textureFilePath))
            {
                return KnownTextures[textureFilePath];
            }

            // If we don't know this texture yet, load it, add it to the dictionary and then return it
            var texture = new Texture(new Bitmap(textureFilePath), textureTarget);
            KnownTextures.Add(textureFilePath, texture);
            return texture;
        }

        /// <summary>
        /// Uploads all textures.
        /// </summary>
        public static void UploadAll()
        {
            KnownTextures.Values.ToList().ForEach(texture => texture.Upload());
        }

        /// <summary>
        /// Unloads all textures.
        /// </summary>
        public static void UnloadAll()
        {
            KnownTextures.Values.ToList().ForEach(texture => texture.Unload());
        }
    }
}