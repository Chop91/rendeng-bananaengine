﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace BananaEngine.Graphics
{
    public class Fbo
    {
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }

        private uint _fbo;
        private uint _depthBuffer;

        public uint[] ColorTextures { get; }
        private int _numberOfColorTextures;

        /// <summary>
        /// Constructor of the frame buffer object.
        /// </summary>
        /// <param name="windowWidth">The width of the window.</param>
        /// <param name="windowHeight">The height of the window.</param>
        /// <param name="numberOfColorTextures">The number of color textures.</param>
        public Fbo(int windowWidth, int windowHeight, int numberOfColorTextures)
        {
            WindowWidth = windowWidth;
            WindowHeight = windowHeight;
            _numberOfColorTextures = numberOfColorTextures;
            ColorTextures = new uint[numberOfColorTextures];
        }

        /// <summary>
        /// Creates the frame buffer object.
        /// </summary>
        /// <returns>True, if everything works. False, if not.</returns>
        public bool CreateFbo()
        {
            // Generate fbo, depth buffer and color texture
            GL.GenFramebuffers(1, out _fbo);
            GL.GenRenderbuffers(1, out _depthBuffer);
            GL.GenTextures(_numberOfColorTextures, ColorTextures);

            // Bind fbo and depth buffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _fbo);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _depthBuffer);


            // Generate color textures
            #region generate color texture

            // Define draw buffers
            var drawBuffers = new DrawBuffersEnum[_numberOfColorTextures];

            // Bind color texture
            for (var i = 0; i < _numberOfColorTextures; i++)
            {
                var colorTexture = ColorTextures[i];

                GL.BindTexture(TextureTarget.Texture2D, colorTexture);

                // Give an empty image to OpenGL ( the last "0" )
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, WindowWidth, WindowHeight, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);

                // Filtering
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

                // Clamp texture
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                // Set render texture as our color attachement #0 + i
                GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, colorTexture, 0);

                // Set the draw buffer
                drawBuffers[i] = DrawBuffersEnum.ColorAttachment0 + i;
            }

            // Set the list of draw buffers
            GL.DrawBuffers(_numberOfColorTextures, drawBuffers);
            #endregion

            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, WindowWidth, WindowHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, _depthBuffer);


            // Always check that our framebuffer is ok
            var error = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (error != FramebufferErrorCode.FramebufferComplete)
            {
                Console.Error.WriteLine(error.ToString());
                return false;
            }

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            return true;
        }

        /// <summary>
        /// Binds the frame buffer object.
        /// </summary>
        public void BindFbo()
        {
            // Render to framebuffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _fbo);
            GL.Viewport(0, 0, WindowWidth, WindowHeight);
        }

        /// <summary>
        /// Unbinds the frame buffer object.
        /// </summary>
        public void UnbindFbo()
        {
            // Render to screen
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, WindowWidth, WindowHeight);
        }

        /// <summary>
        /// Deletes all the opengl related stuff.
        /// </summary>
        public void Delete()
        {
            GL.DeleteTextures(_numberOfColorTextures, ref _numberOfColorTextures);
        }
    }
}