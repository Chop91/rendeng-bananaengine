﻿using System.Collections.Generic;
using System.Linq;

namespace BananaEngine.Graphics
{
    public static class ShaderManager
    {
        private static readonly Dictionary<string, Shader> KnownShaders = new Dictionary<string, Shader>();

        /// <summary>
        /// Gets a shader from a given path. If the texture is already known it simply returns it without loading it again.
        /// </summary>
        /// <param name="vertexShader">The path of the vertex shader.</param>
        /// <param name="fragmentShader">The path of the fragment shader.</param>
        /// <returns>The shader from the given path.</returns>
        public static Shader GetShader(string vertexShader, string fragmentShader)
        {
            return GetShader(vertexShader, null, fragmentShader);
        }

        /// <summary>
        /// Gets a shader from a given path. If the texture is already known it simply returns it without loading it again.
        /// </summary>
        /// <param name="vertexShader">The path of the vertex shader.</param>
        /// <param name="geometryShader">The path of the geometry shader.</param>
        /// <param name="fragmentShader">The path of the fragment shader.</param>
        /// <returns>The shader from the given path.</returns>
        public static Shader GetShader(string vertexShader, string geometryShader, string fragmentShader)
        {
            // If we already know of this texture, simply return it
            if (KnownShaders.ContainsKey(vertexShader))
            {
                return KnownShaders[vertexShader];
            }

            var shader = new Shader(vertexShader, geometryShader, fragmentShader);
            KnownShaders.Add(vertexShader, shader);
            return shader;
        }

        public static void DeleteAll()
        {
            KnownShaders.Values.ToList().ForEach(shader => shader.Unload());
        }
    }
}