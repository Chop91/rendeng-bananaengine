﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace BananaEngine.Graphics
{
    public class Texture
    {
        private readonly Bitmap _textureBitmap;
        private readonly TextureTarget _textureTarget;
        public int? Id { get; set; }

        /// <summary>
        /// Constructor of the texture.
        /// </summary>
        /// <param name="textureBitmap">The bitmap of the texture.</param>
        public Texture(Bitmap textureBitmap)
        {
            Id = null;
            _textureBitmap = textureBitmap;
        }

        /// <summary>
        /// Constructor of the texture.
        /// </summary>
        /// <param name="textureBitmap">The bitmap of the texture.</param>
        /// <param name="textureTarget">The target of the texute.</param>
        public Texture(Bitmap textureBitmap, TextureTarget textureTarget) : this(textureBitmap)
        {
            _textureTarget = textureTarget;
        }

        /// <summary>
        /// Uploads the texture to the GPU without binding it
        /// </summary>
        public void Upload()
        {
            if (Id.HasValue)
            {
                throw new InvalidOperationException("This texture has already been uploaded to the GPU. Reuploading should be considered an error.");
            }

            // Generate texture id
            Id = GL.GenTexture();

            // Bind texture
            GL.BindTexture(_textureTarget, checked((uint)Id));

            // Upload data of the texture
            var textureBitmapData = _textureBitmap.LockBits(new Rectangle(0, 0, _textureBitmap.Width, _textureBitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            if (_textureTarget.Equals(TextureTarget.Texture2D))
            {
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, textureBitmapData.Width, textureBitmapData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, textureBitmapData.Scan0);
            }
            else if(_textureTarget.Equals(TextureTarget.Texture1D))
            {
                GL.TexImage1D(TextureTarget.Texture1D, 0, PixelInternalFormat.Rgba, textureBitmapData.Width, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, textureBitmapData.Scan0);
            }
            _textureBitmap.UnlockBits(textureBitmapData);

            if (_textureTarget.Equals(TextureTarget.Texture2D))
            {
                // This uses mipmap. Hopefully. Because OpenTK docu says:
                // "We will not upload mipmaps, so disable mipmapping (otherwise the texture will not appear)." and uses TextureMinFilter.Linear
                // See: http://www.opentk.com/doc/graphics/textures/loading
                // Generate mipmaps
                GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

                // Filtering
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

                GL.BindTexture(TextureTarget.Texture2D, 0);
            }
            else if (_textureTarget.Equals(TextureTarget.Texture1D))
            {
                // This uses mipmap. Hopefully. Because OpenTK docu says:
                // "We will not upload mipmaps, so disable mipmapping (otherwise the texture will not appear)." and uses TextureMinFilter.Linear
                // See: http://www.opentk.com/doc/graphics/textures/loading
                // Generate mipmaps
                GL.GenerateMipmap(GenerateMipmapTarget.Texture1D);

                // Filtering
                GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
                GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

                GL.BindTexture(TextureTarget.Texture1D, 0);
            }
        }

        /// <summary>
        /// Unloads a texture from the GPU. Not to be confused with unbinding.
        /// </summary>
        public void Unload()
        {
            if (Id.HasValue)
            {
                GL.DeleteTexture(checked((uint)Id));
                Id = null;
            }
        }
    }
}