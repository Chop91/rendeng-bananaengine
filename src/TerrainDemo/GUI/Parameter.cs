﻿using OpenTK;

namespace TerrainDemo.GUI
{
    public class Parameter
    {
        // Parameter for the terrain
        #region terrain

        public bool UseSeed { get; set; }

        public int Seed { get; set; }

        public int Detail { get; set; }

        public float HurstExponent { get; set; }

        public float TerrainScale { get; set; }

        public float MaxTerrainHeight { get; set; }

        public bool Smooth { get; set; }

        public enum FilterType
        {
            Gauss,
            Median
        }

        public FilterType Filter { get; set; }

        public int KernelSize { get; set; }

        public bool Wireframe { get; set; }

        public bool BoundingBox { get; set; }

        #endregion


        // Parameter for the water
        #region water

        public float WaterHeight { get; set; }
        
        public float WaterTransparency { get; set; }

        #endregion


        // Parameter for the light
        #region light

        public Vector3 LightDirection { get; set; }

        #endregion


        // Parameter for the camera
        #region camera

        public float Fov { get; set; }
        public float NearPlane { get; set; }
        public float FarPlane { get; set; }

        #endregion


        // Parameter for the optimization
        #region optimization

        public bool ShowDebugCamera { get; set; }

        public bool ViewFrustumCulling { get; set; }

        public int MaxTrianglesPerQuad { get; set; }

        public float Fps { get; set; }

        public int NumTrianglesRendered { get; set; }

        public int NumTrianglesTotal { get; set; }

        public float RenderEffort { get; set; }

        public int MaxDepthOfQuadTree { get; set; }

        #endregion
    }
}