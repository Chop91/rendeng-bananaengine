﻿using System.Collections.Generic;
using System.Drawing;
using AntTweakBar;
using OpenTK;
using OpenTK.Input;
using TerrainDemo.Geometry;

namespace TerrainDemo.GUI
{
    internal class Gui
    {
        // Constants
        private const float MarginScale = 0.0125f;

        private readonly Context _context;
        private Bar _settingsBar;
        private Size _windowSize;
        private readonly double _renderPeriod;

        private FloatVariable _waterHeight;
        private FloatVariable _fov;
        private FloatVariable _fps;
        private IntVariable _numTrianglesRendered;
        private IntVariable _numTrianglesTotal;
        private FloatVariable _renderEffort;
        private IntVariable _maxDepthOfQuadTree;

        public Parameter Parameter { get; }
        public Scene Scene { get; set; }

        /// <summary>
        /// Constructor of the graphical user interface.
        /// </summary>
        /// <param name="windowSize">The size of the window.</param>
        /// <param name="renderPeriod">The update interval of the graphical user interface.</param>
        public Gui(Size windowSize, double renderPeriod)
        {
            _windowSize = windowSize;
            _renderPeriod = renderPeriod;
            
            _context = new Context(Tw.GraphicsAPI.OpenGLCore);
            Parameter = new Parameter();
            InitializeParameters();
            InitializeSettingsBar();
        }

        /// <summary>
        /// Initializes the parameter with default settings.
        /// </summary>
        private void InitializeParameters()
        {
            // Terrain
            Parameter.UseSeed = true;
            Parameter.Seed = 1337;
            Parameter.Detail = 8;
            Parameter.HurstExponent = 0.6f;
            Parameter.TerrainScale = 1.0f;
            Parameter.Smooth = true;
            Parameter.Filter = Parameter.FilterType.Gauss;
            Parameter.KernelSize = 5;
            Parameter.Wireframe = false;
            Parameter.BoundingBox = true;

            // Water
            Parameter.WaterHeight = 0.5f;
            Parameter.WaterTransparency = 0.7f;

            // Light
            Parameter.LightDirection = new Vector3(0.0f, 0.0f, -1.0f);

            // Camera
            Parameter.Fov = 60.0f;
            Parameter.NearPlane = 0.5f;
            Parameter.FarPlane = 1000.0f;

            // Optimization
            Parameter.ShowDebugCamera = true;
            Parameter.ViewFrustumCulling = false;
            Parameter.MaxTrianglesPerQuad = 10000;
            Parameter.Fps = 0.0f;
            Parameter.NumTrianglesRendered = 0;
            Parameter.NumTrianglesTotal = 0;
            Parameter.RenderEffort = 1.0f;
            Parameter.MaxDepthOfQuadTree = 0;
        }

        /// <summary>
        /// Initializes the settings bar.
        /// </summary>
        private void InitializeSettingsBar()
        {
            _settingsBar = new Bar(_context)
            {
                Label = "Settings",
                Contained = true,
                Iconified = false,
                Help = "Press SPACE to hide/unhide the 'Settings'-Bar.",
                Size = new Size(260, 600),
                ButtonAlignment = BarButtonAlignment.Center,
                Position =
                    new Point((int) (MarginScale * _windowSize.Width),
                        (int) (MarginScale * _windowSize.Height))
            };
            _settingsBar.SetDefinition("refresh=" + _renderPeriod + " color='17 109 143' alpha=64");

            var terrainGroup = new Group(_settingsBar);
            var waterGroup = new Group(_settingsBar);
            var lightGroup = new Group(_settingsBar);
            var cameraGroup = new Group(_settingsBar);
            var optimizationGroup = new Group(_settingsBar);

            #region terrain

            var useSeed = new BoolVariable(_settingsBar, Parameter.UseSeed);
            useSeed.Label = "Seed";
            useSeed.Help = "Enables/Disables the fixed seed (1337) for the random number generator.";
            useSeed.Changed += delegate { Parameter.UseSeed = useSeed.Value; };
            useSeed.Group = terrainGroup;

            var detail = new IntVariable(_settingsBar, Parameter.Detail);
            detail.Label = "Detail";
            detail.Help = "The detail of the terrain.";
            detail.SetDefinition("min=1 max=10 step=1");
            detail.Changed += delegate { Parameter.Detail = detail.Value; };
            detail.Group = terrainGroup;

            var hurstExponent = new FloatVariable(_settingsBar, Parameter.HurstExponent);
            hurstExponent.Label = "Hurst Exponent";
            hurstExponent.Help = "The hurst exponent determines the roughness of the terrain.";
            hurstExponent.SetDefinition("min=0.0 max=1.000 step=0.020 precision=3");
            hurstExponent.Changed += delegate { Parameter.HurstExponent = hurstExponent.Value; };
            hurstExponent.Group = terrainGroup;

            var terrainScale = new FloatVariable(_settingsBar, Parameter.TerrainScale);
            terrainScale.Label = "Terrain Scale";
            terrainScale.Help = "The scale of the terrain (also scales the water since it should always be the same size as the terrain).";
            terrainScale.SetDefinition("min=0.1 max=50.0 step=0.1 precision=1");
            terrainScale.Changed += delegate { Parameter.TerrainScale = terrainScale.Value; };
            terrainScale.Group = terrainGroup;

            var smooth = new BoolVariable(_settingsBar, Parameter.Smooth);
            smooth.Label = "Smooth";
            smooth.Help = "Enables/Disables the smoothing of the terrain.";
            smooth.Changed += delegate { Parameter.Smooth = smooth.Value; Scene.SmoothTerrain(); };
            smooth.Group = terrainGroup;

            var filterType = new EnumVariable<Parameter.FilterType>(_settingsBar, Parameter.Filter);
            filterType.Label = "Filter Type";
            filterType.Help = "The type of the filter to use for the smoothing.";
            filterType.Changed += delegate { Parameter.Filter = filterType.Value; Scene.SmoothTerrain(); };
            filterType.Group = terrainGroup;

            var kernelSize = new IntVariable(_settingsBar, Parameter.KernelSize);
            kernelSize.Label = "Kernelsize";
            kernelSize.Help = "The size of the filter kernel.";
            kernelSize.SetDefinition("min=3 max=25 step = 2");
            kernelSize.Changed += delegate { Parameter.KernelSize = kernelSize.Value; Scene.SmoothTerrain(); };
            kernelSize.Group = terrainGroup;

            var wireframe = new BoolVariable(_settingsBar, Parameter.Wireframe);
            wireframe.Label = "Wireframe";
            wireframe.Help = "Enables/Disables the wireframe mode of the terrain.";
            wireframe.Changed += delegate { Parameter.Wireframe = wireframe.Value; };
            wireframe.Group = terrainGroup;

            var boundingBox = new BoolVariable(_settingsBar, Parameter.BoundingBox);
            boundingBox.Label = "BoundingBox";
            boundingBox.Help = "Enables/Disables the bounding box of the terrain/terrain tiles.";
            boundingBox.Changed += delegate { Parameter.BoundingBox = boundingBox.Value; };
            boundingBox.Group = terrainGroup;

            var buttonGenerate = new Button(_settingsBar);
            buttonGenerate.Label = "Generate Terrain";
            buttonGenerate.Help = "Generates a new terrain with the defined settings above.";
            buttonGenerate.Clicked +=delegate { Scene.LoadTerrain(); };
            buttonGenerate.Group = terrainGroup;

            #endregion


            #region water

            _waterHeight = new FloatVariable(_settingsBar, Parameter.WaterHeight);
            _waterHeight.Label = "Water Height";
            _waterHeight.Help = "The height of the water.";
            _waterHeight.SetDefinition("min=-5000.0 max=5000.0 step=0.50 precision=1");
            _waterHeight.Changed += delegate { Parameter.WaterHeight = _waterHeight.Value; };
            _waterHeight.Group = waterGroup;

            var waterTransparency = new FloatVariable(_settingsBar, Parameter.WaterTransparency);
            waterTransparency.Label = "Water Transparency";
            waterTransparency.Help = "The transparency of the water.";
            waterTransparency.SetDefinition("min=0.0 max=1.000 step=0.020 precision=3");
            waterTransparency.Changed += delegate { Parameter.WaterTransparency = waterTransparency.Value; };
            waterTransparency.Group = waterGroup;

            #endregion


            #region light

            // Light
            var lightDirection = new VectorVariable(_settingsBar, Parameter.LightDirection.X, Parameter.LightDirection.Y, Parameter.LightDirection.Z);
            lightDirection.Label = "Light Direction";
            lightDirection.Help = "The direction of the light.";
            lightDirection.SetDefinition("opened=true");
            lightDirection.ShowValue = true;
            lightDirection.Changed += delegate { Parameter.LightDirection = new Vector3(lightDirection.X, lightDirection.Y, lightDirection.Z); };
            lightDirection.Group = lightGroup;

            #endregion


            #region camera

            _fov = new FloatVariable(_settingsBar, Parameter.Fov);
            _fov.Label = "Field of View";
            _fov.Help = "The field of view of the camera.";
            _fov.SetDefinition("min=15.0 max=90.0 step=0.5 precision=1");
            _fov.Changed += delegate { Parameter.Fov = _fov.Value; Scene.UpdateCameraParameter(); };
            _fov.Group = cameraGroup;

            var nearPlane = new FloatVariable(_settingsBar, Parameter.NearPlane);
            nearPlane.Label = "Near Plane";
            nearPlane.Help = "The near plane of the camera";
            nearPlane.SetDefinition("min=0.5 max=50.0 step=0.5 precision=1");
            nearPlane.Changed += delegate { Parameter.NearPlane = nearPlane.Value; Scene.UpdateCameraParameter(); };
            nearPlane.Group = cameraGroup;

            var farPlane = new FloatVariable(_settingsBar, Parameter.FarPlane);
            farPlane.Label = "Far Plane";
            farPlane.Help = "The far plane of the camera.";
            farPlane.SetDefinition("min=100.0 max=50000.0 step=100.0 precision=1");
            farPlane.Changed += delegate { Parameter.FarPlane = farPlane.Value; Scene.UpdateCameraParameter(); };
            farPlane.Group = cameraGroup;

            #endregion


            #region optimization

            var showDebugCamera = new BoolVariable(_settingsBar, Parameter.ShowDebugCamera);
            showDebugCamera.Label = "Show Debug Camera";
            showDebugCamera.Help = "Enables/Disables the debug camera.";
            showDebugCamera.Changed += delegate { Parameter.ShowDebugCamera = showDebugCamera.Value; };
            showDebugCamera.Group = optimizationGroup;

            var viewFrustumCulling = new BoolVariable(_settingsBar, Parameter.ViewFrustumCulling);
            viewFrustumCulling.Label = "View Frustum Culling";
            viewFrustumCulling.Help = "Enables/Disables the view frustum culling.";
            viewFrustumCulling.Changed += delegate { Parameter.ViewFrustumCulling = viewFrustumCulling.Value; };
            viewFrustumCulling.Group = optimizationGroup;

            var maxTrianglesPerQuad = new IntVariable(_settingsBar, Parameter.MaxTrianglesPerQuad);
            maxTrianglesPerQuad.Label = "Max Triangles per Quad";
            maxTrianglesPerQuad.Help = "The maximum number of triangles per quad (condition to create child nodes).";
            maxTrianglesPerQuad.SetDefinition("min=2500 max=200000 step=250");
            maxTrianglesPerQuad.Changed += delegate { Parameter.MaxTrianglesPerQuad = maxTrianglesPerQuad.Value; };
            maxTrianglesPerQuad.Group = optimizationGroup;

            var buttonGenerateQuadtree = new Button(_settingsBar);
            buttonGenerateQuadtree.Label = "Generate Quadtree";
            buttonGenerateQuadtree.Help = "Generates a new quadtree with the defined settings above.";
            buttonGenerateQuadtree.Clicked += delegate { Scene.LoadQuadTree(); };
            buttonGenerateQuadtree.Group = optimizationGroup;

            _fps = new FloatVariable(_settingsBar, Parameter.Fps);
            _fps.Label = "FPS";
            _fps.Help = "Shows the FPS.";
            _fps.SetDefinition("precision=1");
            _fps.Changed += delegate { Parameter.Fps = _fps.Value; };
            _fps.ReadOnly = true;
            _fps.Group = optimizationGroup;

            _numTrianglesRendered = new IntVariable(_settingsBar, Parameter.NumTrianglesRendered);
            _numTrianglesRendered.Label = "#Triangles Rendered";
            _numTrianglesRendered.Help = "Shows the number of rendered triangles of the terrain.";
            _numTrianglesRendered.Changed += delegate { Parameter.NumTrianglesRendered = _numTrianglesRendered.Value; };
            _numTrianglesRendered.ReadOnly = true;
            _numTrianglesRendered.Group = optimizationGroup;

            _numTrianglesTotal = new IntVariable(_settingsBar, Parameter.NumTrianglesTotal);
            _numTrianglesTotal.Label = "#Triangles Total";
            _numTrianglesTotal.Help = "Shows the total number of triangles of the terrain.";
            _numTrianglesTotal.Changed += delegate { Parameter.NumTrianglesTotal = _numTrianglesTotal.Value; };
            _numTrianglesTotal.ReadOnly = true;
            _numTrianglesTotal.Group = optimizationGroup;

            _renderEffort = new FloatVariable(_settingsBar, Parameter.RenderEffort);
            _renderEffort.Label = "Render Effort";
            _renderEffort.Help = "The 'Render Effort' is defined as #numTrianglesCulled / #numTrianglesTotal.";
            _renderEffort.SetDefinition("precision=2");
            _renderEffort.Changed += delegate { Parameter.RenderEffort = _renderEffort.Value; };
            _renderEffort.ReadOnly = true;
            _renderEffort.Group = optimizationGroup;

            _maxDepthOfQuadTree = new IntVariable(_settingsBar, Parameter.MaxDepthOfQuadTree);
            _maxDepthOfQuadTree.Label = "Max Depth of Quadtree";
            _maxDepthOfQuadTree.Help = "The maximum depth of the Quadtree";
            _maxDepthOfQuadTree.Changed += delegate { Parameter.MaxDepthOfQuadTree = _maxDepthOfQuadTree.Value; };
            _maxDepthOfQuadTree.ReadOnly = true;
            _maxDepthOfQuadTree.Group = optimizationGroup;

            #endregion


            terrainGroup.Label = "Terrain";
            waterGroup.Label = "Water";
            lightGroup.Label = "Light";
            cameraGroup.Label = "Camera";
            optimizationGroup.Label = "Optimization";
        }

        #region Minor event handling

        /// <summary>
        /// Updates the graphical user interface.
        /// </summary>
        public void OnUpdateFrame()
        {
            // updates some values of the gui
            _fps.Value = Parameter.Fps;
            _numTrianglesRendered.Value = Parameter.NumTrianglesRendered;
            _numTrianglesTotal.Value = Parameter.NumTrianglesTotal;
            _renderEffort.Value = Parameter.RenderEffort;
            _maxDepthOfQuadTree.Value = Parameter.MaxDepthOfQuadTree;
            _fov.Value = Parameter.Fov;

            _waterHeight.Min = -Parameter.MaxTerrainHeight;
            _waterHeight.Max = Parameter.MaxTerrainHeight;
        }

        /// <summary>
        /// Renders the graphical user interface.
        /// </summary>
        public void OnRenderFrame()
        {
            _context.Draw();
        }

        public void OnResize(Size clientSize)
        {
            _context.HandleResize(clientSize);
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void OnMouseDown(MouseButtonEventArgs e)
        {
            if (HandleMouseClick(_context, e))
            {
                return;
            }
        }

        public void OnMouseUp(MouseButtonEventArgs e)
        {
            if (HandleMouseClick(_context, e))
            {
                return;
            }
        }

        public void OnMouseMove(MouseMoveEventArgs e)
        {
            if (_context.HandleMouseMove(e.Position))
            {
                return;
            }
        }

        public void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (_context.HandleMouseWheel(e.Value))
            {
                return;
            }
        }


        public void OnKeyDown(KeyboardKeyEventArgs e)
        {
            // Shortcut to iconify the settings bar
            if (e.Key.Equals(Key.Space))
            {
                _settingsBar.Iconified = !_settingsBar.Iconified;
            }

            if (HandleKeyInput(_context, e, true))
            {
                return;
            }
        }

        public void OnKeyUp(KeyboardKeyEventArgs e)
        {
            if (HandleKeyInput(_context, e, false))
            {
                return;
            }
        }

        public void OnKeyPress(KeyPressEventArgs e)
        {
            if (_context.HandleKeyPress(e.KeyChar))
            {
                return;
            }
        }

        #endregion

        // ATTENTION: COPIED FROM https://github.com/TomCrypto/AntTweakBar.NET/blob/master/Samples/OpenTK-OpenGL/Program.cs
        #region Event Conversion

        // Because OpenTK does not use an event loop, the native AntTweakBar library
        // has no provisions for directly handling user events. Therefore we need to
        // convert any OpenTK events to AntTweakBar events before handling them.

        private static bool HandleMouseClick(Context context, MouseButtonEventArgs e)
        {
            IDictionary<MouseButton, Tw.MouseButton> mapping = new Dictionary<MouseButton, Tw.MouseButton>()
            {
                { MouseButton.Left, Tw.MouseButton.Left },
                { MouseButton.Middle, Tw.MouseButton.Middle },
                { MouseButton.Right, Tw.MouseButton.Right },
            };

            var action = e.IsPressed ? Tw.MouseAction.Pressed : Tw.MouseAction.Released;

            if (mapping.ContainsKey(e.Button))
            {
                return context.HandleMouseClick(action, mapping[e.Button]);
            }
            else
            {
                return false;
            }
        }

        private static bool HandleKeyInput(Context context, KeyboardKeyEventArgs e, bool down)
        {
            IDictionary<Key, Tw.Key> mapping = new Dictionary<Key, Tw.Key>()
            {
                { Key.BackSpace, Tw.Key.Backspace },
                { Key.Tab, Tw.Key.Tab },
                { Key.Clear, Tw.Key.Clear },
                { Key.Enter, Tw.Key.Return },
                { Key.Pause, Tw.Key.Pause },
                { Key.Escape, Tw.Key.Escape },
                { Key.Space, Tw.Key.Space },
                { Key.Delete, Tw.Key.Delete },
                { Key.Up, Tw.Key.Up },
                { Key.Down, Tw.Key.Down },
                { Key.Right, Tw.Key.Right },
                { Key.Left, Tw.Key.Left },
                { Key.Insert, Tw.Key.Insert },
                { Key.Home, Tw.Key.Home },
                { Key.End, Tw.Key.End },
                { Key.PageUp, Tw.Key.PageUp },
                { Key.PageDown, Tw.Key.PageDown },
                { Key.F1, Tw.Key.F1 },
                { Key.F2, Tw.Key.F2 },
                { Key.F3, Tw.Key.F3 },
                { Key.F4, Tw.Key.F4 },
                { Key.F5, Tw.Key.F5 },
                { Key.F6, Tw.Key.F6 },
                { Key.F7, Tw.Key.F7 },
                { Key.F8, Tw.Key.F8 },
                { Key.F9, Tw.Key.F9 },
                { Key.F10, Tw.Key.F10 },
                { Key.F11, Tw.Key.F11 },
                { Key.F12, Tw.Key.F12 },
                { Key.F13, Tw.Key.F13 },
                { Key.F14, Tw.Key.F14 },
                { Key.F15, Tw.Key.F15 },
                { Key.A, Tw.Key.A },
                { Key.B, Tw.Key.B },
                { Key.C, Tw.Key.C },
                { Key.D, Tw.Key.D },
                { Key.E, Tw.Key.E },
                { Key.F, Tw.Key.F },
                { Key.G, Tw.Key.G },
                { Key.H, Tw.Key.H },
                { Key.I, Tw.Key.I },
                { Key.J, Tw.Key.J },
                { Key.K, Tw.Key.K },
                { Key.L, Tw.Key.L },
                { Key.M, Tw.Key.M },
                { Key.N, Tw.Key.N },
                { Key.O, Tw.Key.O },
                { Key.P, Tw.Key.P },
                { Key.Q, Tw.Key.Q },
                { Key.R, Tw.Key.R },
                { Key.S, Tw.Key.S },
                { Key.T, Tw.Key.T },
                { Key.U, Tw.Key.U },
                { Key.V, Tw.Key.V },
                { Key.W, Tw.Key.W },
                { Key.X, Tw.Key.X },
                { Key.Y, Tw.Key.Y },
                { Key.Z, Tw.Key.Z },
            };

            var modifiers = Tw.KeyModifiers.None;
            if (e.Modifiers.HasFlag(KeyModifiers.Alt))
                modifiers |= Tw.KeyModifiers.Alt;
            if (e.Modifiers.HasFlag(KeyModifiers.Shift))
                modifiers |= Tw.KeyModifiers.Shift;
            if (e.Modifiers.HasFlag(KeyModifiers.Control))
                modifiers |= Tw.KeyModifiers.Ctrl;

            if (mapping.ContainsKey(e.Key))
            {
                if (down)
                {
                    return context.HandleKeyDown(mapping[e.Key], modifiers);
                }
                else
                {
                    return context.HandleKeyUp(mapping[e.Key], modifiers);
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }

    internal sealed class PointVariable : StructVariable<Vector3>
    {
        private FloatVariable X => (variables[0] as FloatVariable);
        private FloatVariable Y => (variables[1] as FloatVariable);
        private FloatVariable Z => (variables[2] as FloatVariable);


        public PointVariable(Bar bar, Vector3 point)
            : base(bar, new FloatVariable(bar, point.X), new FloatVariable(bar, point.Y), new FloatVariable(bar, point.Z))
        {
            X.Label = "X";
            Y.Label = "Y";
            Z.Label = "Z";
        }

        public override Vector3 Value
        {
            get { return new Vector3(X.Value, Y.Value, Z.Value); }
            set
            {
                X.Value = value.X;
                Y.Value = value.Y;
                Z.Value = value.Z;
            }
        }

        public float Step
        {
            get { return X.Step; }
            set
            {
                X.Step = value;
                Y.Step = value;
                Z.Step = value;
            }
        }

        public float Precision
        {
            get { return X.Precision; }
            set
            {
                X.Precision = value;
                Y.Precision = value;
                Z.Precision = value;
            }
        }
    }
}