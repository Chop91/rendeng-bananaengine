﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using TerrainDemo.GUI;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using TerrainDemo.Geometry;
using ClearBufferMask = OpenTK.Graphics.OpenGL4.ClearBufferMask;
using EnableCap = OpenTK.Graphics.OpenGL4.EnableCap;
using GL = OpenTK.Graphics.OpenGL4.GL;

namespace TerrainDemo
{
    internal class Renderer : GameWindow
    {
        // Constants
        private const float MinFov = 15.0f;
        private const float MaxFov = 90.0f;
        private const float FovStep = 0.5f;
        private const int NumberOfKeysPressedSimultaneously = 3;

        private float _deltaTime;
        private List<Key> _keyList;
        private bool _lockCursor;

        private Matrix4 _rotX;
        private Matrix4 _rotY;
        private Gui _gui;
        private Scene _scene;

        /// <summary>
        /// Constructor of the renderer.
        /// </summary>
        public Renderer()
            : base(800, 600, new GraphicsMode(32, 24, 0, 4))
        {
            // Get data from App.config
            var windowWidth = Convert.ToInt32(ConfigurationManager.AppSettings["windowWidth"]);
            var windowHeight = Convert.ToInt32(ConfigurationManager.AppSettings["windowHeight"]);
            var fullscreen = Convert.ToBoolean(ConfigurationManager.AppSettings["fullscreen"]);
            var renderFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["renderFrequency"]);
            var updateFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["updateFrequency"]);
            var iconPath = Convert.ToString(ConfigurationManager.AppSettings["iconPath"]);
            var windowName = Convert.ToString(ConfigurationManager.AppSettings["windowName"]);

            ClientSize = new Size(windowWidth, windowHeight);
            Title = windowName;
            Icon = new Icon(iconPath);

            if (fullscreen)
            {
                WindowState = WindowState.Fullscreen;
                TargetRenderFrequency = renderFrequency;
                TargetUpdateFrequency = updateFrequency;
            }
        }

        /// <summary>
        /// Loads the scene and initializes the renderer.
        /// </summary>
        /// <param name="e">Event argument</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.ClearColor(0, 0, 0, 0);
            GL.Enable(EnableCap.DepthTest);
            WindowBorder = WindowBorder.Fixed;


            _gui = new Gui(ClientSize, TargetRenderPeriod);
            _keyList = new List<Key>();
            CursorVisible = false;
            _lockCursor = true;

            _rotX = Matrix4.Identity;
            _rotY = Matrix4.Identity;

            _scene = new Scene(ClientSize, _gui.Parameter);
            _gui.Scene = _scene;
        }

        /// <summary>
        /// Updates everything.
        /// </summary>
        /// <param name="e">Frame event argument.</param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            _deltaTime = Convert.ToSingle(e.Time);
            ExecuteKeyList(_deltaTime);

            // Calculate transformation
            var transformationMatrix = _rotY * _rotX;
            _scene.OnUpdateFrame(transformationMatrix, RenderFrequency);
            _gui.OnUpdateFrame();
        }

        /// <summary>
        /// Renders everything.
        /// </summary>
        /// <param name="e">Frame event argument.</param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Render Scene
            _scene.OnRenderFrame();

            // Render GUI
            _gui.OnRenderFrame();

            SwapBuffers();
        }

        /// <summary>
        /// Exits the demo and deletes all the opengl stuff.
        /// </summary>
        public override void Exit()
        {
            _scene.Exit();
            base.Exit();
        }


        #region Minor event handling

        // According to suggestion from here to handle multiple keys at once: http://neokabuto.blogspot.co.at/2014/01/opentk-tutorial-5-basic-camera.html
        private void ExecuteKeyList(float deltaTime)
        {
            foreach (var key in _keyList)
            {
                switch (key)
                {
                    case Key.W:
                        _scene.MainCamera.MoveForward(deltaTime);
                        break;
                    case Key.S:
                        _scene.MainCamera.MoveBackward(deltaTime);
                        break;
                    case Key.A:
                        _scene.MainCamera.StrafeLeft(deltaTime);
                        break;
                    case Key.D:
                        _scene.MainCamera.StrafeRight(deltaTime);
                        break;
                    case Key.R:
                        _scene.MainCamera.MoveUp(deltaTime);
                        break;
                    case Key.F:
                        _scene.MainCamera.MoveDown(deltaTime);
                        break;
                    case Key.Up:
                        _rotX *= Matrix4.CreateRotationX((float)(Math.PI / 2.0f) * deltaTime);
                        break;
                    case Key.Down:
                        _rotX *= Matrix4.CreateRotationX((float)(-Math.PI / 2.0f) * deltaTime);
                        break;
                    case Key.Left:
                        _rotY *= Matrix4.CreateRotationY((float)(Math.PI / 2.0f) * deltaTime);
                        break;
                    case Key.Right:
                        _rotY *= Matrix4.CreateRotationY((float)(-Math.PI / 2.0f) * deltaTime);
                        break;
                }
            }

            //keyList.Clear();
        }

        private Vector2 _lastMousePosition;

        private void ResetCursor()
        {
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
            _lastMousePosition = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            if (Focused && _lockCursor)
            {
                ResetCursor();
            }
        }

        

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            GL.Viewport(ClientRectangle);
            _gui.OnResize(ClientSize);
        }

        protected override void Dispose(bool manual)
        {
            _gui.Dispose();
            base.Dispose(manual);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _gui.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            _gui.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
            _gui.OnMouseMove(e);

            if (Focused && _lockCursor)
            {
                var delta = _lastMousePosition - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                delta = -delta;
                _scene.MainCamera.UpdateViewDirection(delta, _deltaTime);

                ResetCursor();
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            _gui.OnMouseWheel(e);

            var fov = _scene.MainCamera.Fov - e.Delta * FovStep;
            if (fov >= MinFov && fov <= MaxFov)
            {
                _scene.MainCamera.Fov = fov;
            }
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);

            _gui.OnKeyDown(e);

            if (e.Key == Key.Escape)
            {
                Close();
            }
            else if (e.Key == Key.C)
            {
                _lockCursor = !_lockCursor;
                CursorVisible = !CursorVisible;
            }
            else if (_keyList.Count <= NumberOfKeysPressedSimultaneously)
            {
                _keyList.Add(e.Key);
            }
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            base.OnKeyUp(e);
            _gui.OnKeyUp(e);

            _keyList.RemoveAll(x => x == e.Key);
            
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            _gui.OnKeyPress(e);
        }

        #endregion
    }
}