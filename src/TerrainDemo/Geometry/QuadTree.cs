﻿using System;
using System.Collections.Generic;
using BananaEngine.Geometry;
using BananaEngine.Geometry.Models.Primitives._3D;
using OpenTK;
using TerrainDemo.GUI;

namespace TerrainDemo.Geometry
{
    internal class QuadTree
    {
        private const int NumChildren = 4;
        private int _totalNumberOfNodes;

        private List<Mesh.Vertex> _vertices;
        private Node _rootNode;

        private int _triangleCount;
        public int DrawCount { get; private set; }
        public int NumTrianglesTotal { get; set; }

        private readonly Parameter _parameter;

        public struct Node
        {
            public float PositionX;
            public float PositionZ;
            public float Width;
            public int TriangleCount;
            public TerrainTile TerrainTile;
            public Node?[] Nodes;
        }

        /// <summary>
        /// Constructor of the quad tree.
        /// </summary>
        /// <param name="parameter">The parameter to use.</param>
        public QuadTree(Parameter parameter)
        {
            _parameter = parameter;
        }

        /// <summary>
        /// Initializes the quadtree for the provided terrain.
        /// </summary>
        /// <param name="terrain">The terrain to create the quad tree for.</param>
        public void Initialize(Terrain terrain)
        {
            float centerX;
            float centerZ;
            float width;
            var vertexCount = terrain.VertexCount;
            _triangleCount = vertexCount / 3;
            _totalNumberOfNodes = 0;
            NumTrianglesTotal = 0;

            _vertices = new List<Mesh.Vertex>();
            foreach (var vertex in terrain.Mesh.Vertices)
            {
                var v = new Mesh.Vertex
                {
                    Position = vertex.Position,
                    Uv = vertex.Uv,
                    Normal = vertex.Normal,
                    Color = vertex.Color
                };
                _vertices.Add(v);
            }

            // Calculate the center x, z and the width of the mesh
            CalculateMeshDimensions(vertexCount, out centerX, out centerZ , out width);

            // Create the rootNode node for the quad tree
            _rootNode = new Node();

            // Recursively build the quad tree based on the vertex list data and mesh dimensions
            CreateTreeNode(ref _rootNode, centerX, centerZ, width, terrain);

            // Calculate the maximum depth
            _parameter.MaxDepthOfQuadTree = (int) Math.Ceiling(Math.Log(_totalNumberOfNodes) / Math.Log(NumChildren));
        }

        /// <summary>
        /// Renders the quadtree.
        /// </summary>
        /// <param name="terrain">The terrain.</param>
        /// <param name="rotationsMatrix">The roation matrix.</param>
        /// <param name="light">The light to use.</param>
        /// <param name="parameter">The parameter to use.</param>
        /// <param name="camera">The camera to use.</param>
        /// <param name="viewFrustum">The view frustum of the camera.</param>
        public void Render(Terrain terrain, Matrix4 rotationsMatrix, Light light, Parameter parameter, Camera camera, ViewFrustum viewFrustum)
        {
            DrawCount = 0;

            // Render each node that is visible starting at the rootNode node and moving down the tree
            RenderNode(ref _rootNode, terrain, rotationsMatrix, light, camera, viewFrustum);
        }

        /// <summary>
        /// Calculates the dimensions of the mesh.
        /// </summary>
        /// <param name="vertexCount">The number of vertices.</param>
        /// <param name="centerX">The center x position.</param>
        /// <param name="centerZ">The center z position.</param>
        /// <param name="meshWidth">The width of the mesh.</param>
        private void CalculateMeshDimensions(int vertexCount, out float centerX, out float centerZ, out float meshWidth)
        {
            // Initialize the center position of the mesh to zero
            centerX = 0.0f;
            centerZ = 0.0f;

            foreach (var vertex in _vertices)
            {
                centerX += vertex.Position.X;
                centerZ += vertex.Position.Z;
            }

            // And then divide it by the number of vertices to find the mid-point of the mesh
            centerX = centerX / (float)vertexCount;
            centerZ = centerZ / (float)vertexCount;

            // Initialize the maximum and minimum size of the mesh
            var maxWidth = 0.0f;
            var maxDepth = 0.0f;

            var minWidth = Math.Abs(_vertices[0].Position.X - centerX);
            var minDepth = Math.Abs(_vertices[0].Position.Z - centerZ);

            // Go through all the vertices and find the maximum and minimum width and depth of the mesh
            for (var i = 0; i < vertexCount; i++)
            {
                var width = Math.Abs(_vertices[i].Position.X - centerX);
                var depth = Math.Abs(_vertices[i].Position.Z - centerZ);

                if (width > maxWidth) { maxWidth = width; }
                if (depth > maxDepth) { maxDepth = depth; }
                if (width < minWidth) { minWidth = width; }
                if (depth < minDepth) { minDepth = depth; }
            }

            // Find the absolute maximum value between the min and max depth and width
            var maxX = (float)Math.Max(Math.Abs(minWidth), Math.Abs(maxWidth));
            var maxZ = (float)Math.Max(Math.Abs(minDepth), Math.Abs(maxDepth));

            // Calculate the maximum diameter of the mesh
            meshWidth = Math.Max(maxX, maxZ) * 2.0f;
        }

        /// <summary>
        /// Creates a tree node.
        /// </summary>
        /// <param name="node">The current node.</param>
        /// <param name="positionX">The x position.</param>
        /// <param name="positionZ">The y position.</param>
        /// <param name="width">The width.</param>
        /// <param name="terrain">The terrain.</param>
        private void CreateTreeNode(ref Node node, float positionX, float positionZ, float width, Terrain terrain)
        {
            node.PositionX = positionX;
            node.PositionZ = positionZ;
            node.Width = width;
            node.TriangleCount = 0;
            node.Nodes = new Node?[NumChildren];

            // Count the number of triangles that are inside this node
            var numTriangles = CountTriangles(positionX, positionZ, width);

            // Case 1: If there are no triangles in this node then return as it is empty and requires no processing
            if (numTriangles == 0)
            {
                return;
            }
            // Case 2: If there are too many triangles in this node then split it into four equal sized smaller tree nodes
            if (numTriangles > _parameter.MaxTrianglesPerQuad)
            {
                for (var i = 0; i < NumChildren; i++)
                {
                    // Calculate the position offsets for the new child node
                    var offsetX = (i % 2 < 1 ? -1.0f : 1.0f) * (width / 4.0f);
                    var offsetZ = (i % 4 < 2 ? -1.0f : 1.0f) * (width / 4.0f);

                    // See if there are any triangles in the new node
                    var count = CountTriangles(positionX + offsetX, positionZ + offsetZ, width / 2.0f);
                    if (count > 0)
                    {
                        // If there are triangles inside where this new node would be then create the child node
                        node.Nodes[i] = new Node();

                        // Extend the tree starting from this new child node now
                        var value = node.Nodes[i].Value;
                        CreateTreeNode(ref value, positionX + offsetX, positionZ + offsetZ, width / 2.0f, terrain);
                        node.Nodes[i] = value;
                    }
                }

                return;
            }


            // Case 3: If this node is not empty and the triangle count for it is less than the max then 
            // this node is at the bottom of the tree so create the list of triangles to store in it
            node.TriangleCount = numTriangles;
            var vertexCount = numTriangles * 3;
            var vertices = new List<Mesh.Vertex>(vertexCount);
            var indices = new List<int>(vertexCount);
            var index = 0;

            for (var i = 0; i < _triangleCount; i++)
            {
                // If the triangle is inside this node then add it to the vertex array
                var result = IsTriangleContained(i, positionX, positionZ, width);
                if (result)
                {
                    // Calculate the index into the terrain vertex list
                    var vertexIndex = i * 3;

                    // Get the three vertices of this triangle from the vertex list
                    var vertex = new Mesh.Vertex
                    {
                        Position = _vertices[vertexIndex].Position,
                        Uv = _vertices[vertexIndex].Uv,
                        Normal = _vertices[vertexIndex].Normal,
                        Color = _vertices[vertexIndex].Color
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    vertexIndex++;
                    vertex = new Mesh.Vertex
                    {
                        Position = _vertices[vertexIndex].Position,
                        Uv = _vertices[vertexIndex].Uv,
                        Normal = _vertices[vertexIndex].Normal,
                        Color = _vertices[vertexIndex].Color
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    vertexIndex++;
                    vertex = new Mesh.Vertex
                    {
                        Position = _vertices[vertexIndex].Position,
                        Uv = _vertices[vertexIndex].Uv,
                        Normal = _vertices[vertexIndex].Normal,
                        Color = _vertices[vertexIndex].Color
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;
                }
            }

            node.TerrainTile = new TerrainTile(vertices, indices);
            node.TerrainTile.ModelMatrix = terrain.ModelMatrix;
            node.TerrainTile.Mesh.BoundingBox.ModelMatrix = terrain.ModelMatrix;
            _totalNumberOfNodes++;
            NumTrianglesTotal += node.TriangleCount;
        }

        /// <summary>
        /// Counts the number of triangles that are inside this node.
        /// </summary>
        /// <param name="positionX">The x position.</param>
        /// <param name="positionZ">The y position.</param>
        /// <param name="width">The width.</param>
        /// <returns></returns>
        private int CountTriangles(float positionX, float positionZ, float width)
        {
            var count = 0;

            for (var i = 0; i < _triangleCount; i++)
            {
                // If the triangle is inside the node then increment the count by one
                var result = IsTriangleContained(i, positionX, positionZ, width);
                if (result)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Checks if the triangle is contained in the node.
        /// </summary>
        /// <param name="index">The current index</param>
        /// <param name="positionX">The x position.</param>
        /// <param name="positionZ">The y position.</param>
        /// <param name="width">The width.</param>
        /// <returns>True if it is contained, otherwise false.</returns>
        private bool IsTriangleContained(int index, float positionX, float positionZ, float width)
        {
            // Calculate the radius of this node
            var radius = width / 2.0f;

            // Get the index into the vertex list
            var vertexIndex = index * 3;

            // Get the three vertices of this triangle from the vertex list
            var x1 = _vertices[vertexIndex].Position.X;
            var z1 = _vertices[vertexIndex].Position.Z;
            vertexIndex++;

            var x2 = _vertices[vertexIndex].Position.X;
            var z2 = _vertices[vertexIndex].Position.Z;
            vertexIndex++;

            var x3 = _vertices[vertexIndex].Position.X;
            var z3 = _vertices[vertexIndex].Position.Z;

            // Check to see if the minimum of the x coordinates of the triangle is inside the node
            var minimumX = Math.Min(x1, Math.Min(x2, x3));
            if (minimumX > (positionX + radius))
            {
                return false;
            }

            // Check to see if the maximum of the x coordinates of the triangle is inside the node
            var maximumX = Math.Max(x1, Math.Max(x2, x3));
            if (maximumX < (positionX - radius))
            {
                return false;
            }

            // Check to see if the minimum of the z coordinates of the triangle is inside the node
            var minimumZ = Math.Min(z1, Math.Min(z2, z3));
            if (minimumZ > (positionZ + radius))
            {
                return false;
            }

            // Check to see if the maximum of the z coordinates of the triangle is inside the node
            var maximumZ = Math.Max(z1, Math.Max(z2, z3));
            if (maximumZ < (positionZ - radius))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Renders the current node if it is a leaf.
        /// </summary>
        /// <param name="node">The node to render.</param>
        /// <param name="terrain">The terrain.</param>
        /// <param name="rotationsMatrix">The roation matrix.</param>
        /// <param name="light">The light to use.</param>
        /// <param name="camera">The camera to use.</param>
        /// <param name="viewFrustum">The view frustum of the camera.</param>
        private void RenderNode(ref Node node, Terrain terrain, Matrix4 rotationsMatrix, Light light, Camera camera, ViewFrustum viewFrustum)
        {
            // Check to see if the node can be viewed, height doesn't matter in a quad tree
            if (_parameter.ViewFrustumCulling && node.TerrainTile != null && node.TerrainTile.Mesh != null)
            {
                var trans = Matrix4.CreateTranslation(-new Vector3(0.5f));
                var rot = rotationsMatrix;
                var scale = Matrix4.CreateScale(_parameter.TerrainScale * 500);
                
                var transformationMatrix = trans * rot * scale; // T * R * S
                var model = terrain.ModelMatrix * transformationMatrix;
                var result = viewFrustum.BoundingBoxInFrustum(node.TerrainTile.Mesh.BoundingBox, model);

                // Node is outside
                if (!result)
                {
                    return;
                }
            }

            // If it can be seen then check all four child nodes to see if they can also be seen
            var count = 0;
            for (var i = 0; i < 4; i++)
            {
                if (node.Nodes != null && node.Nodes[i].HasValue)
                {
                    count++;
                    var value = node.Nodes[i].Value;
                    RenderNode(ref value, terrain, rotationsMatrix, light, camera, viewFrustum);
                }
            }

            // If there were any children nodes then there is no need to continue as rootNode nodes won't contain any triangles to render
            if (count != 0)
            {
                return;
            }

            if (node.TerrainTile != null && node.TerrainTile.Mesh != null)
            {
                terrain.Render(rotationsMatrix, light, node.TerrainTile.Mesh, _parameter, camera);

                // Increase the count of the number of polygons that have been rendered during this frame.
                DrawCount += node.TriangleCount;
            }
        }

        /// <summary>
        /// Deletes the quadtree.
        /// </summary>
        public void Delete()
        {
            Delete(_rootNode);
        }

        /// <summary>
        /// Deletes the opengl stuff of the leafs.
        /// </summary>
        /// <param name="node">The current node.</param>
        private void Delete(Node node)
        {
            var count = 0;
            for (var i = 0; i < 4; i++)
            {
                if (node.Nodes != null && node.Nodes[i].HasValue)
                {
                    count++;
                    Delete(node.Nodes[i].Value);
                }
            }

            // If there are no more children, finally delete the mesh of the tiles
            if (count == 0)
            {
                node.TerrainTile.Delete();
            }
        }
    }
}
