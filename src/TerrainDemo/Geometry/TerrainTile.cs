﻿using System.Collections.Generic;
using BananaEngine.Geometry.Models.Primitives._3D;
using BananaEngine.Graphics;

namespace TerrainDemo.Geometry
{
    internal class TerrainTile : Model
    {
        /// <summary>
        /// Constructor of the terrain tile.
        /// </summary>
        /// <param name="vertices">The vertices of the terrain tile.</param>
        /// <param name="indices">The indices of the terrain tile.</param>
        public TerrainTile(List<Mesh.Vertex> vertices, List<int> indices)
        {
            Mesh = new Mesh(vertices, indices, true);

            // Load dirt texture
            const string path = @"./Data/dirt.png";
            Mesh.TextureDiffuse = TextureManager.GetTextureFromFilePath(path);
        }
    }
}