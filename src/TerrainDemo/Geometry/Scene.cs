﻿using System;
using BananaEngine.Util;
using System.Drawing;
using BananaEngine.Geometry;
using BananaEngine.Graphics;
using Emgu.CV;
using Emgu.CV.Structure;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using TerrainDemo.GUI;

namespace TerrainDemo.Geometry
{
    internal class Scene
    {
        // Models
        private Light _light;
        private Terrain _terrain;
        private QuadTree _quadTree;
        private Water _water;
        private SkyBox _skyBox;
        private Image<Gray, byte> _heightMap;

        // Camera
        public Camera MainCamera { get; set; }
        private ViewFrustum _viewFrustum;

        // Debug tools
        private Camera _debugCamera;
        private Fbo _debugFbo;
        private FullscreenRenderer _fullscreenRenderer;

        // Other variables
        private readonly Parameter _parameter;
        private Size _windowSize;
        private Matrix4 _transformationMatrix;


        /// <summary>
        /// Constructor of the scene.
        /// </summary>
        /// <param name="windowSize">The size of the window.</param>
        /// <param name="parameter">The parameters from the GUI.</param>
        public Scene(Size windowSize, Parameter parameter)
        {
            _windowSize = windowSize;
            _parameter = parameter;

            LoadScene();
        }

        /// <summary>
        /// Loads the scene.
        /// </summary>
        public void LoadScene()
        {
            // Camera
            MainCamera = new Camera(_parameter.Fov, _parameter.NearPlane, _parameter.FarPlane, _windowSize);
            _viewFrustum = new ViewFrustum(MainCamera);

            // Debug tools
            _debugCamera = new Camera(60.0f, 0.5f, 200000.0f, _windowSize);
            _debugFbo = new Fbo(_windowSize.Width, _windowSize.Height, 1);
            _debugFbo.CreateFbo();
            _fullscreenRenderer = new FullscreenRenderer();

            // Models
            _water = new Water();
            _skyBox = new SkyBox();
            _light = new Light();
            LoadTerrain();
            //LoadQuadTree();
            TextureManager.UploadAll();
        }

        /// <summary>
        /// Updates the camera parameters.
        /// </summary>
        public void UpdateCameraParameter()
        {
            MainCamera.Fov = _parameter.Fov;
            MainCamera.NearPlane = _parameter.NearPlane;
            MainCamera.FarPlane = _parameter.FarPlane;
        }

        /// <summary>
        /// Smooths the terrain.
        /// </summary>
        public void SmoothTerrain()
        {
            var smoothedHeightMap = TerrainGenerator.SmoothHeightMap(_heightMap, _parameter);
            _terrain = new Terrain(smoothedHeightMap, _parameter);
            LoadQuadTree();
        }

        /// <summary>
        /// Loads the terrain.
        /// </summary>
        public void LoadTerrain()
        {
            Console.WriteLine("Start generating terrain.");
            _heightMap = TerrainGenerator.GenerateTerrain(_parameter);
            Console.WriteLine("Finished generating terrain.");

            if (_parameter.Smooth)
            {
                SmoothTerrain();
            }
            else
            {
                _terrain = new Terrain(_heightMap, _parameter);
                LoadQuadTree();
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Loads the quad tree.
        /// </summary>
        public void LoadQuadTree()
        {
            Console.WriteLine("Start generating quadtree.");

            _quadTree = new QuadTree(_parameter);
            _quadTree.Initialize(_terrain);

            Console.WriteLine("Finished generating quadtree.");
        }

        /// <summary>
        /// Updates the scene.
        /// </summary>
        /// <param name="transformationMatrix">The transformation matrix calculated according to the user input.</param>
        /// <param name="renderFrequency">The render frequency.</param>
        public void OnUpdateFrame(Matrix4 transformationMatrix, double renderFrequency)
        {
            // Update camera
            MainCamera.GenerateModelAndViewAndProjection();
            var followMainCamera = Matrix4.Invert(Matrix4.LookAt(new Vector3(500, 500, 500) * _parameter.TerrainScale, MainCamera.Position, MainCamera.UpVector));
            _debugCamera.GenerateModelAndViewAndProjection(followMainCamera);

            // Update light
            _light.LightDirection = _parameter.LightDirection;

            // Update transformation
            _transformationMatrix = transformationMatrix;

            // Update parameter
            _parameter.Fps = (float) Math.Round(renderFrequency, 1);
            _parameter.NumTrianglesTotal = _quadTree.NumTrianglesTotal;
            _parameter.NumTrianglesRendered = _quadTree.DrawCount;
            _parameter.RenderEffort = (float) _parameter.NumTrianglesRendered / _parameter.NumTrianglesTotal;
            _parameter.Fov = MainCamera.Fov;
        }

        /// <summary>
        /// Renders the scene.
        /// </summary>
        public void OnRenderFrame()
        {
            RenderMainCamera();

            if (_parameter.ShowDebugCamera)
            {
                RenderDebugCamera();
            }
        }

        /// <summary>
        /// Renders the main camera.
        /// </summary>
        private void RenderMainCamera()
        {
            // Render the skybox
            _skyBox.Render(MainCamera);

            // Render the terrain and blend it with the water
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            _quadTree.Render(_terrain, _transformationMatrix, _light, _parameter, MainCamera, _viewFrustum);

            // Render the water
            _water.Render(MainCamera, _parameter);

            GL.Disable(EnableCap.Blend);
        }

        /// <summary>
        /// Renders the debug camera.
        /// </summary>
        private void RenderDebugCamera()
        {
            _debugFbo.BindFbo();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Render the skybox
            _skyBox.Render(_debugCamera);

            // Render the terrain and blend it with the water
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            _quadTree.Render(_terrain, _transformationMatrix, _light, _parameter, _debugCamera, _viewFrustum);

            // Render the water
            _water.Render(_debugCamera, _parameter);

            GL.Disable(EnableCap.Blend);

            // Render the view frustum
            _viewFrustum.Render(_debugCamera);
            _debugFbo.UnbindFbo();

            GL.Disable(EnableCap.DepthTest);

            _fullscreenRenderer.RenderBottomRight(_debugFbo.ColorTextures[0]);

            GL.Enable(EnableCap.DepthTest);
        }

        /// <summary>
        /// Exits the scene and deletes all the opengl stuff.
        /// </summary>
        public void Exit()
        {
            // Release everything on the GPU
            _terrain.Delete();
            _skyBox.Delete();
            _water.Delete();
            _quadTree.Delete();
            _viewFrustum.Delete();

            TextureManager.UnloadAll();
            ShaderManager.DeleteAll();
            _debugFbo.Delete();
            _fullscreenRenderer.Delete();
        }
    }
}