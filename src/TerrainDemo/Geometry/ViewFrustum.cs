﻿using System;
using BananaEngine.Geometry;
using BananaEngine.Geometry.Models.Primitives._3D;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace TerrainDemo.Geometry
{
    internal sealed class ViewFrustum : Cube
    {
        private readonly Camera _camera;

        /// <summary>
        /// Constructor of the view frustum.
        /// </summary>
        /// <param name="camera">The corresponding camera.</param>
        public ViewFrustum(Camera camera)
        {
            _camera = camera;
            Shader = ShaderManager.GetShader("./Shader/solid.vert", "./Shader/solid.frag");
        }

        /// <summary>
        /// Renders the view frustum with the view of a debug camera.
        /// </summary>
        /// <param name="debugCamera">The debug camera.</param>
        public void Render(Camera debugCamera)
        {
            Shader.UseShader();

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

            // Bind matrices
            GL.BindVertexArray(Mesh.Vao);

            // Use the inverse VP matrix to transform the vertices of the cube to the actual position in the view frustum of the main camera
            var model = Matrix4.Invert(_camera.ViewProjectionMatrix);
            GL.UniformMatrix4(1, false, ref model);
            GL.UniformMatrix4(2, false, ref debugCamera.ViewProjectionMatrix);

            // Draw mesh
            GL.DrawElements(PrimitiveType.Quads, Mesh.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        /// <summary>
        /// Checks if all vertices of the bounding box are outside of the view frustum.
        /// </summary>
        /// <param name="boundingBox">The bounding box.</param>
        /// <param name="model">The model matrix.</param>
        /// <returns>True if inside and false if outside.</returns>
        public bool BoundingBoxInFrustum(BoundingBox boundingBox, Matrix4 model)
        {
            var planesCounter = new[] {0, 0, 0, 0, 0, 0};
            var mvp = model * _camera.ViewProjectionMatrix;

            for (var i = 0; i < 8; i++)
            {
                // Transform vertex to clip space
                var position = boundingBox.Mesh.Vertices[i].Position;
                position = Vector4.Transform(position, mvp);

                // Check if the vertex is outside
                if (position.X > position.W) planesCounter[0]++;
                if (position.X < -position.W) planesCounter[1]++;
                if (position.Y > position.W) planesCounter[2]++;
                if (position.Y < -position.W) planesCounter[3]++;
                if (position.Z > position.W) planesCounter[4]++;
                if (position.Z < -position.W) planesCounter[5]++;
            }

            for (var i = 0; i < 6; i++)
            {
                //Outside
                if (planesCounter[i] == 8)
                {
                    return false;
                }
            }
            //Inside
            return true;
        }
    }
}