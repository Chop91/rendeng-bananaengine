﻿using System;
using BananaEngine.Geometry;
using BananaEngine.Geometry.Models.Primitives._3D;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using TerrainDemo.GUI;

namespace TerrainDemo.Geometry
{
    internal sealed class Water : Quad3D
    {
        /// <summary>
        /// Constructor of the water.
        /// </summary>
        public Water()
        {
            Shader = ShaderManager.GetShader("./Shader/water.vert", "./Shader/water.frag");
            Setup();

            // Load water texture
            var path = @"./Data/water.jpg";
            Mesh.TextureDiffuse = TextureManager.GetTextureFromFilePath(path);
        }

        /// <summary>
        /// Renders the water.
        /// </summary>
        /// <param name="camera">The camera.</param>
        /// <param name="parameter">The parameters from the GUI.</param>
        public void Render(Camera camera, Parameter parameter)
        {
            Shader.UseShader();

            // Create transformation
            var waterheight = parameter.WaterHeight / parameter.MaxTerrainHeight;
            var trans = Matrix4.CreateTranslation(new Vector3(0.0f, waterheight, 0.0f));
            var rot = Matrix4.Identity;
            var scale = Matrix4.CreateScale(parameter.TerrainScale * 500 * 0.5f); // MIND: Use the same scale as in the terrain (except the 0.5f)
            var transformationMatrix = trans * rot * scale;

            TransformedModelMatrix = ModelMatrix * transformationMatrix;
            var mvp = TransformedModelMatrix * camera.ViewProjectionMatrix;

            // Bind matrix
            GL.BindVertexArray(Mesh.Vao);
            GL.UniformMatrix4(1, false, ref mvp);

            // Bind texture
            if (Mesh.TextureDiffuse.Id != null)
            {
                GL.Uniform1(2, 0);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, checked((uint)Mesh.TextureDiffuse.Id));
            }

            // Bind transparency
            GL.Uniform1(3, parameter.WaterTransparency);

            // Draw mesh
            GL.DrawElements(PrimitiveType.Triangles, Mesh.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);
        }
    }
}