﻿using System;
using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;
using MathNet.Numerics.Distributions;
using TerrainDemo.GUI;

namespace TerrainDemo.Geometry
{
    public class TerrainGenerator
    {
        private static int _detail;
        private static float _hurstExponent;

        private static float[,] _terrain;
        private static int _size;
        private static int _max;

        private static Random _random;
        private static float _variance;
        private static float _standardDeviation;
        private static int _n;
        
        /// <summary>
        /// Generates the a terrain with the predefined parameters.
        /// </summary>
        /// <param name="parameter">The parameter for the terrain.</param>
        /// <returns>The generated terrain.</returns>
        public static Image<Gray, byte> GenerateTerrain(Parameter parameter)
        {
            _detail = parameter.Detail;
            _hurstExponent = parameter.HurstExponent;
            _random = parameter.UseSeed ? new Random(parameter.Seed) : new Random();
            _n = 0;
            _variance = 1.0f;

            _size = (int)(Math.Pow(2, _detail) + 1);
            _max = _size - 1;
            _terrain = new float[_size, _size];

            // 1. Set the corners in the middle of th z-axis of the cube.
            _terrain[0, 0] = _max / 2.0f;
            _terrain[_max, 0] = _max / 2.0f;
            _terrain[_max, _max] = _max / 2.0f;
            _terrain[0, _max] = _max / 2.0f;

            // 2. Divide the map
            Divide(_max);

            // 3. Generate Heightmap
            var heightMap = GenerateHeightMap();
            return heightMap;
        }

        /// <summary>
        /// Smooths the terrain by using a filter kernel. The provided parameters contain the type and kernel size.
        /// </summary>
        /// <param name="heightMap">The height map to smooth.</param>
        /// <param name="parameter">The parameters containing the filter type and kernel size.</param>
        /// <returns></returns>
        public static Image<Gray, byte> SmoothHeightMap(Image<Gray, byte> heightMap, Parameter parameter)
        {
            if (!parameter.Smooth)
            {
                return heightMap;
            }

            var img = heightMap;

            if (parameter.Filter.Equals(Parameter.FilterType.Gauss))
            {
                img = img.SmoothGaussian(parameter.KernelSize);
            }
            else if (parameter.Filter.Equals(Parameter.FilterType.Median))
            {
                img = img.SmoothMedian(parameter.KernelSize);
            }

            return img;
        }

        /// <summary>
        /// Generates a height map image from the computed two dimensional terrain array.
        /// </summary>
        /// <returns></returns>
        private static Image<Gray, byte> GenerateHeightMap()
        {
            var matrix = new Matrix<float>(_terrain);
            var img = matrix.Mat.ToImage<Gray, byte>();
            return img;
        }

        /// <summary>
        /// Divides the terrain and performs the actual Diamond and Square algorithm.
        /// </summary>
        /// <param name="size">The size of the last subdivision.</param>
        private static void Divide(int size)
        {
            // Calculate standard deviation
            _standardDeviation = (float) Math.Sqrt(_variance / Math.Pow(2.0f, _n * _hurstExponent));
            _n++;

            var half = size / 2;
            var scale = _hurstExponent * size;

            if (half < 1) return;


            // Diamond step
            for (var y = half; y < _max; y += size)
            {
                for (var x = half; x < _max; x += size)
                {
                    // Generate random number Dn
                    var dN = Normal.Sample(_random, 0.0, _standardDeviation);
                    Diamond(x, y, half, (float)(dN * scale));
                }
            }

            // Square step
            for (var y = 0; y <= _max; y += half)
            {
                for (var x = (y + half) % size; x <= _max; x += size)
                {
                    // Generate random number Dn
                    var dN = Normal.Sample(_random, 0.0, _standardDeviation);
                    Square(x, y, half, (float)(dN * scale));
                }
            }

            Divide(size / 2);
        }

        /// <summary>
        /// Square step. Gets the four surrounding vertices T, R, B, L and returns the average of them translated by an random offset.
        /// </summary>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <param name="size">The size to move.</param>
        /// <param name="offset">The random offset to translate.</param>
        private static void Square(int x, int y, int size, float offset)
        {
            var list = new List<float>
            {
                GetOrDefault(x, y - size), // top
                GetOrDefault(x + size, y), // right
                GetOrDefault(x, y + size), // bottom
                GetOrDefault(x - size, y)  // left
            };
            var average = list.Average();
            _terrain[x, y] = average + offset;
        }

        /// <summary>
        /// Diamond step. Gets the four surrounding vertices UL, UR, LR, LL and returns the average of them translated by an random offset.
        /// </summary>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <param name="size">The size to move.</param>
        /// <param name="offset">The random offset to translate.</param>
        private static void Diamond(int x, int y, int size, float offset)
        {
            var list = new List<float>
            {
                GetOrDefault(x - size, y - size), // upper left
                GetOrDefault(x + size, y - size), // upper right
                GetOrDefault(x + size, y + size), // lower right
                GetOrDefault(x - size, y + size) // lower left
            };
            var average = list.Average();
            _terrain[x, y] = average + offset;
        }

        /// <summary>
        /// Gets the height value of the terrain at position (x, y). If the index is out of range it returns 0.
        /// </summary>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <returns>The height value of the terrain at position (x, y). If the index is out of range it returns 0.</returns>
        private static float GetOrDefault(int x, int y)
        {
            if (x < 0 || x > _max || y < 0 || y > _max) return 0;
            return _terrain[x, y];
        }
    }
}