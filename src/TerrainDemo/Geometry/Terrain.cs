﻿using System;
using System.Collections.Generic;
using BananaEngine.Geometry;
using BananaEngine.Geometry.Models.Primitives._3D;
using BananaEngine.Graphics;
using Emgu.CV;
using Emgu.CV.Structure;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using TerrainDemo.GUI;

namespace TerrainDemo.Geometry
{
    internal class Terrain : Model
    {
        private int _terrainWidth;
        private int _terrainHeight;
        public int VertexCount { get; private set; }
        public int IndexCount { get; set; }


        private List<HeightMap> _heightMap;
        private readonly Parameter _parameter;

        private const int TextureRepeat = 16;

        private readonly Image<Gray, byte> _heightMapImage;

        public struct HeightMap
        {
            public float X;
            public float Y;
            public float Z;
            public float U;
            public float V;
            public float NormalX;
            public float NormalY;
            public float NormalZ;
        }

        /// <summary>
        /// Constructor of the terrain.
        /// </summary>
        /// <param name="heightMapImage">The height map to create the terrain from.</param>
        /// <param name="parameter">The parameter for the terrain.</param>
        public Terrain(Image<Gray, byte> heightMapImage, Parameter parameter)
        {
            Shader = ShaderManager.GetShader("./Shader/terrain.vert", "./Shader/terrain.frag");
            _heightMapImage = heightMapImage;
            _parameter = parameter;
            parameter.MaxTerrainHeight = heightMapImage.Mat.GetValueRange().Max;

            Setup();

            // Load dirt texture
            const string path = @"./Data/dirt.png";
            Mesh.TextureDiffuse = TextureManager.GetTextureFromFilePath(path);
        }

        /// <summary>
        /// Sets the terrain up.
        /// </summary>
        private void Setup()
        {
            // Loads the height map
            LoadHeightMap();

            // Normalizes the height map
            NormalizeHeightMap();

            // Calculates smooth normals
            CalculateNormals();

            // Calculates texture coordinates
            CalculateTextureCoordinates();


            var vertices = new List<Mesh.Vertex>();
            var indices = new List<int>();

            // Calculate the number of vertices in the terrain mesh
            VertexCount = (_terrainWidth - 1) * (_terrainHeight - 1) * 6;

            var index = 0;
            for (var j = 0; j < _terrainHeight - 1; j++)
            {
                for (var i = 0; i < _terrainWidth - 1; i++)
                {
                    var index1 = _terrainHeight * j + i;          // Bottom left.
                    var index2 = _terrainHeight * j + i + 1;      // Bottom right.
                    var index3 = _terrainHeight * (j + 1) + i;      // Upper left.
                    var index4 = _terrainHeight * (j + 1) + i + 1;  // Upper right.


                    // Upper left
                    var v = _heightMap[index3].V;
                    // Modify the texture coordinates to cover the top edge
                    if (Math.Abs(v - 1.0f) < float.Epsilon)
                    {
                        v = 0.0f;
                    }

                    var vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index3].X, _heightMap[index3].Y, _heightMap[index3].Z, 1.0f),
                        Uv = new Vector2(_heightMap[index3].U, v),
                        Normal = new Vector3(_heightMap[index3].NormalX, _heightMap[index3].NormalY,_heightMap[index3].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    // Upper right.
                    var u = _heightMap[index4].U;
                    v = _heightMap[index4].V;

                    // Modify the texture coordinates to cover the top and right edge
                    if (Math.Abs(u) < float.Epsilon) { u = 1.0f; }
                    if (Math.Abs(v - 1.0f) < float.Epsilon) { v = 0.0f; }

                    vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index4].X, _heightMap[index4].Y, _heightMap[index4].Z, 1.0f),
                        Uv = new Vector2(u, v),
                        Normal = new Vector3(_heightMap[index4].NormalX, _heightMap[index4].NormalY,_heightMap[index4].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    // Bottom left
                    vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index1].X, _heightMap[index1].Y, _heightMap[index1].Z, 1.0f),
                        Uv = new Vector2(_heightMap[index1].U, _heightMap[index1].V),
                        Normal = new Vector3(_heightMap[index1].NormalX, _heightMap[index1].NormalY, _heightMap[index1].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    // Bottom left
                    vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index1].X, _heightMap[index1].Y, _heightMap[index1].Z, 1.0f),
                        Uv = new Vector2(_heightMap[index1].U, _heightMap[index1].V),
                        Normal = new Vector3(_heightMap[index1].NormalX, _heightMap[index1].NormalY, _heightMap[index1].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;

                    // Upper right
                    u = _heightMap[index4].U;
                    v = _heightMap[index4].V;

                    // Modify the texture coordinates to cover the top and right edge
                    if (Math.Abs(u) < float.Epsilon) { u = 1.0f; }
                    if (Math.Abs(v - 1.0f) < float.Epsilon) { v = 0.0f; }

                    vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index4].X, _heightMap[index4].Y, _heightMap[index4].Z, 1.0f),
                        Uv = new Vector2(u, v),
                        Normal = new Vector3(_heightMap[index4].NormalX, _heightMap[index4].NormalY, _heightMap[index4].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;


                    // Bottom right
                    u = _heightMap[index2].U;
                    // Modify the texture coordinates to cover the right edge.
                    if (Math.Abs(u) < float.Epsilon) { u = 1.0f; }

                    vertex = new Mesh.Vertex
                    {
                        Position = new Vector4(_heightMap[index2].X, _heightMap[index2].Y, _heightMap[index2].Z, 1.0f),
                        Uv = new Vector2(u, _heightMap[index2].V),
                        Normal = new Vector3(_heightMap[index2].NormalX, _heightMap[index2].NormalY, _heightMap[index2].NormalZ),
                        Color = Vector3.One
                    };
                    vertices.Add(vertex);
                    indices.Add(index);
                    index++;
                }
            }

            Mesh = new Mesh(vertices, indices);
            IndexCount = Mesh.Indices.Count;
        }

        /// <summary>
        /// Normalizes the height of the height map.
        /// </summary>
        private void NormalizeHeightMap()
        {
            for (var i = 0; i < _heightMap.Count; i++)
            {
                var heightMap = _heightMap[i];
                heightMap.X /= _terrainWidth;
                heightMap.Y /= _parameter.MaxTerrainHeight;
                heightMap.Z /= _terrainHeight;
                _heightMap[i] = heightMap;
            }
        }

        /// <summary>
        /// Calculates smooth normals.
        /// </summary>
        private void CalculateNormals()
        {
            var normals = new List<Vector3>();

            for (var j = 0; j < _terrainHeight - 1; j++)
            {
                for (var i = 0; i < _terrainWidth - 1; i++)
                {
                    var index1 = j * _terrainHeight + i;
                    var index2 = j * _terrainHeight + i + 1;
                    var index3 = (j + 1) * _terrainHeight + i;

                    // Get three vertices from the face
                    var vertex1 = new Vector3
                    {
                        X = _heightMap[index1].X,
                        Y = _heightMap[index1].Y,
                        Z = _heightMap[index1].Z
                    };

                    var vertex2 = new Vector3
                    {
                        X = _heightMap[index2].X,
                        Y = _heightMap[index2].Y,
                        Z = _heightMap[index2].Z
                    };

                    var vertex3 = new Vector3
                    {
                        X = _heightMap[index3].X,
                        Y = _heightMap[index3].Y,
                        Z = _heightMap[index3].Z
                    };

                    // Calculate the two vectors for this face
                    var vector1 = new Vector3
                    {
                        X = vertex1.X - vertex3.X,
                        Y = vertex1.Y - vertex3.Y,
                        Z = vertex1.Z - vertex3.Z,
                    };

                    var vector2 = new Vector3
                    {
                        X = vertex3.X - vertex2.X,
                        Y = vertex3.Y - vertex2.Y,
                        Z = vertex3.Z - vertex2.Z,
                    };

                    // Calculate the cross product of those two vectors to get the un-normalized value for this face normal
                    var normal = Vector3.Cross(vector1, vector2);
                    normals.Add(normal);
                }
            }

            for (var j = 0; j < _terrainHeight; j++)
            {
                for (var i = 0; i < _terrainWidth; i++)
                {
                    // Initialize the sum
                    var sum = Vector3.Zero;

                    // Initialize the count
                    var count = 0;

                    // Bottom left face
                    if (i - 1 >= 0 && j - 1 >= 0)
                    {
                        var index = (j - 1) * (_terrainHeight - 1) + (i - 1);
                        sum = Vector3.Add(sum, normals[index]);
                        count++;
                    }

                    // Bottom right face
                    if (i < _terrainWidth - 1 && j - 1 >= 0)
                    {
                        var index = (j - 1) * (_terrainHeight - 1) + i;
                        sum = Vector3.Add(sum, normals[index]);
                        count++;
                    }

                    // Upper left face
                    if (i - 1 >= 0 && j < _terrainHeight - 1)
                    {
                        var index = j * (_terrainHeight - 1) + (i - 1);
                        sum = Vector3.Add(sum, normals[index]);
                        count++;
                    }

                    // Upper right face
                    if (i < _terrainWidth - 1 && j < _terrainHeight - 1)
                    {
                        var index = j * (_terrainHeight - 1) + i;
                        sum = Vector3.Add(sum, normals[index]);
                        count++;
                    }

                    // Take the average of the faces touching this vertex
                    sum = Vector3.Divide(sum, count);

                    // Get an index to the vertex location in the height map array
                    var index2 = j * _terrainHeight + i;

                    // Normalize the final shared normal for this vertex and store it in the height map array
                    var normalizedNormal = Vector3.Normalize(sum);
                    var heightMap = _heightMap[index2];
                    heightMap.NormalX = normalizedNormal.X;
                    heightMap.NormalY = normalizedNormal.Y;
                    heightMap.NormalZ = normalizedNormal.Z;
                    _heightMap[index2] = heightMap;
                }
            }
        }
        
        /// <summary>
        /// Calculates the texture coordinates.
        /// </summary>
        private void CalculateTextureCoordinates()
        {
            var incrementValue = (float) TextureRepeat / _terrainWidth;
            var incrementCount = _terrainWidth / TextureRepeat;

            // Initializes the u and v coordinate values
            var uCoordinate = 0.0f;
            var vCoordinate = 1.0f;

            // Initialize the u and v coordinate indexes
            var uCount = 0;
            var vCount = 0;

            for (var j = 0; j < _terrainHeight; j++)
            {
                for (var i = 0; i < _terrainWidth; i++)
                {
                    // Store the texture coordinate in the height map
                    var heightMap = _heightMap[_terrainHeight * j + i];
                    heightMap.U = uCoordinate;
                    heightMap.V = vCoordinate;
                    _heightMap[_terrainHeight * j + i] = heightMap;

                    uCoordinate += incrementValue;
                    uCount++;

                    // Check if at the far right end of the texture and if so then start at the beginning again
                    if (uCount == incrementCount)
                    {
                        uCoordinate = 0.0f;
                        uCount = 0;
                    }

                }

                vCoordinate -= incrementValue;
                vCount++;

                // Check if at the top of the texture and if so then start at the bottom again
                if (vCount == incrementCount)
                {
                    vCoordinate = 1.0f;
                    vCount = 0;
                }
            }
        }

        /// <summary>
        /// Loads the height map image in the data structure.
        /// </summary>
        private void LoadHeightMap()
        {
            var bitmap = _heightMapImage.Bitmap;
            _terrainWidth = bitmap.Width;
            _terrainHeight = bitmap.Height;

            _heightMap = new List<HeightMap>(_terrainHeight * _terrainWidth);

            for (var j = 0; j < _terrainHeight; j++)
            {
                for (var i = 0; i < _terrainWidth; i++)
                {
                    int height = bitmap.GetPixel(i, j).R;

                    HeightMap heightMap;
                    heightMap.X = i;
                    heightMap.Y = height;
                    heightMap.Z = j;
                    heightMap.U = 0.0f;
                    heightMap.V = 0.0f;
                    heightMap.NormalX = 0.0f;
                    heightMap.NormalY = 0.0f;
                    heightMap.NormalZ = 0.0f;

                    _heightMap.Add(heightMap);
                }
            }   
        }

        /// <summary>
        /// Renders the terrain and bounding box.
        /// </summary>
        /// <param name="rotationMatrix">The rotation matrix.</param>
        /// <param name="light">The light to use.</param>
        /// <param name="mesh">The mesh to render.</param>
        /// <param name="parameter">The parameter to use.</param>
        /// <param name="camera">The camera to use.</param>
        public void Render(Matrix4 rotationMatrix, Light light, Mesh mesh, Parameter parameter, Camera camera)
        {
            // Create transformation
            var trans = Matrix4.CreateTranslation(-new Vector3(0.5f));
            var rot = rotationMatrix;
            var scale = Matrix4.CreateScale(parameter.TerrainScale * 500); // MIND: Use the same scale as in the water (except the 0.5f)
            var transformationMatrix = trans * rot * scale; // T * R * S
            mesh.BoundingBox.TransformedModelMatrix = mesh.BoundingBox.ModelMatrix * transformationMatrix;
            TransformedModelMatrix = ModelMatrix * transformationMatrix;

            if (parameter.Wireframe)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            }

            RenderTerrain(light, mesh, camera);

            if (parameter.Wireframe)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            }

            if (parameter.BoundingBox)
            {
                mesh.BoundingBox.Render(camera, transformationMatrix);
            }
        }

        /// <summary>
        /// Renders the terrain.
        /// </summary>
        /// <param name="light">The light to use.</param>
        /// <param name="mesh">The mesh to render.</param>
        /// <param name="camera">The camera to use.</param>
        private void RenderTerrain(Light light, Mesh mesh, Camera camera)
        {
            Shader.UseShader();
            GL.BindVertexArray(mesh.Vao);

            // Bind matrices
            GL.UniformMatrix4(1, false, ref TransformedModelMatrix);
            GL.UniformMatrix4(2, false, ref camera.ViewMatrix);
            GL.UniformMatrix4(3, false, ref camera.ViewProjectionMatrix);

            // Bind texture
            if (mesh.TextureDiffuse.Id != null)
            {
                GL.Uniform1(4, 0);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, checked((uint)mesh.TextureDiffuse.Id));
            }

            // Bind light
            var startLocation = 7;
            var shininess = 1.0f;
            GL.Uniform3(startLocation + 0, light.LightDirection);
            GL.Uniform3(startLocation + 1, new Vector3(light.ColorAmbient.R, light.ColorAmbient.G, light.ColorAmbient.B));
            GL.Uniform3(startLocation + 2, new Vector3(light.ColorDiffuse.R, light.ColorDiffuse.G, light.ColorDiffuse.B));
            GL.Uniform3(startLocation + 3, new Vector3(light.ColorSpecular.R, light.ColorSpecular.G, light.ColorSpecular.B));
            GL.Uniform1(startLocation + 4, light.AttentuationConstant);
            GL.Uniform1(startLocation + 5, light.AttentuationLinear);
            GL.Uniform1(startLocation + 6, light.AttentuationQuadratic);
            GL.Uniform1(startLocation + 7, shininess);

            // Draw mesh
            GL.DrawElements(PrimitiveType.Triangles, mesh.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);
        }
    }
}