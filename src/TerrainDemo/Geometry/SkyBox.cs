﻿using System;
using BananaEngine.Geometry;
using BananaEngine.Geometry.Models.Primitives._3D;
using BananaEngine.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace TerrainDemo.Geometry
{
    internal class SkyBox : Cube
    {
        private readonly CubeMapFbo _cubeMapFbo;
        private const int CubeMapSize = 1024;
        private const float SkyBoxScale = 0.5f;

        /// <summary>
        /// Constructor of the skybox.
        /// </summary>
        public SkyBox()
        {
            Shader = ShaderManager.GetShader("./Shader/skybox.vert", "./Shader/skybox.frag");
            Setup();
            _cubeMapFbo = new CubeMapFbo(CubeMapSize, CubeMapSize);
            _cubeMapFbo.CreateFbo(true);
        }

        /// <summary>
        /// Renders the skybox.
        /// </summary>
        /// <param name="camera">The camera.</param>
        public void Render(Camera camera)
        {
            Shader.UseShader();

            // Update transformed model matrix
            var translation = Matrix4.CreateTranslation(camera.Position);
            var scale = Matrix4.CreateScale(SkyBoxScale * camera.FarPlane);
            var transformationMatrix = scale * translation;
            TransformedModelMatrix = ModelMatrix * transformationMatrix;

            // Bind matrices
            GL.BindVertexArray(Mesh.Vao);
            GL.UniformMatrix4(1, false, ref TransformedModelMatrix);
            GL.UniformMatrix4(2, false, ref camera.ViewProjectionMatrix);

            // Bind camera
            var cameraPosition = camera.Position;
            GL.Uniform3(3, ref cameraPosition);

            // Bind texture
            GL.Uniform1(4, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.TextureCubeMap, checked((uint) _cubeMapFbo.ColorTexture));

            // Draw mesh
            GL.DrawElements(PrimitiveType.Quads, Mesh.Indices.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            GL.BindVertexArray(0);
        }

        /// <summary>
        /// Deletes all the opengl stuff.
        /// </summary>
        protected internal new void Delete()
        {
            base.Delete();
            _cubeMapFbo.Delete();
        }
    }
}