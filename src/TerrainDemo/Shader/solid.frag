#version 430 core

in vec4 f_color;

layout(location = 0) out vec4 o_color;

void main()
{
	o_color = f_color;
}