#version 430 core

layout(location = 1) in vec4 v_position;
layout(location = 2) in vec3 v_normal;
layout(location = 3) in vec2 v_uv;
layout(location = 4) in vec3 v_color;

out vec4 f_color;

layout(location = 1) uniform mat4 u_M;
layout(location = 2) uniform mat4 u_VP;

void main()
{
	// Positions
	gl_Position = u_VP * u_M * v_position;

	// Color
	f_color = vec4(v_color, 1.0);
}