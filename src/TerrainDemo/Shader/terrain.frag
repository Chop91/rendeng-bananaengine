#version 430 core

in vec2 f_uv;
in vec3 f_object_normal;
in vec3 f_object_to_light_direction;

layout(location = 0) out vec4 o_color;

struct Light {
	vec3 light_direction;
	vec3 light_color_ambient;
	vec3 light_color_diffuse;
	vec3 light_color_specular;
	float attenuation_constant;
	float attenuation_linear;
	float attenuation_quadratic;
	float shininess;
};

layout(location = 4) uniform sampler2D u_texture_diffuse;
layout(location = 7) uniform Light u_light;

float ka = 0.75f;
float kd = 1.0f;
float ks = 0.0f; // no specular highlights for terrain.

void main()
{
	vec3 material_diffuse_color = texture(u_texture_diffuse, f_uv).rgb;
	vec3 material_ambient_color = material_diffuse_color * vec3(1.0, 0.97, 0.85);
	vec3 out_color_rgb = vec3(0.0, 0.0, 0.0);

	// Normalize the vectors or strange things will happen
	vec3 n = normalize(f_object_normal);
	vec3 l = normalize(f_object_to_light_direction);

	// Calculation cos between normal and light
	float cos_theta = clamp(dot(n, l), 0.0, 1.0);

	// Do the shading
	vec3 color = kd * material_diffuse_color * u_light.light_color_diffuse * cos_theta;

	// Apply ambient light here because it should not be affected by light attenuation
	color += ka * material_ambient_color * u_light.light_color_ambient;

	// Accumulate the color
	out_color_rgb += color;

	// Handle spillover
	vec3 spillover = max(vec3(0.0, 0.0, 0.0), out_color_rgb - vec3(1.0, 1.0, 1.0));
	out_color_rgb.r += spillover.g / 2.0 + spillover.b / 2.0;
	out_color_rgb.g += spillover.r / 2.0 + spillover.b / 2.0;
	out_color_rgb.b += spillover.r / 2.0 + spillover.g / 2.0;
	
	o_color = vec4(out_color_rgb, 1.0);
}