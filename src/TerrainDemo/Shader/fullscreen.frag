﻿#version 430 core

in vec2 f_uv;

layout(location = 0) out vec4 o_color;

layout(location = 2) uniform sampler2D u_texture_diffuse;

void main()
{
	// Get material colors
	vec3 material_diffuse_color = texture(u_texture_diffuse, f_uv).rgb;
	
	o_color = vec4(material_diffuse_color, 0.0);
}