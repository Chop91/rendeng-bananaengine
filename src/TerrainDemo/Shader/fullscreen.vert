﻿#version 430 core

layout(location = 1) in vec2 v_position;
layout(location = 2) in vec2 v_uv;


out vec2 f_uv;

layout(location = 1) uniform mat4 u_M;
layout(location = 3) uniform float v_heightcrop;

void main()
{
	// Positions
	gl_Position = u_M * vec4(vec2(1.0, v_heightcrop) * v_position, 0.0, 1.0);

	// Uvs
	f_uv = vec2(1.0, v_heightcrop) * v_uv + vec2(0.0, (1.0 - v_heightcrop) /2.0);
}