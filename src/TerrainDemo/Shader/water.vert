#version 430 core

layout(location = 1) in vec4 v_position;
layout(location = 2) in vec3 v_normal;
layout(location = 3) in vec2 v_uv;
layout(location = 4) in vec3 v_color;

out vec2 f_uv;

layout(location = 1) uniform mat4 u_MVP;

void main()
{
	// Positions
	gl_Position = u_MVP * v_position;

	// Uvs
	f_uv = v_uv * 4; // Multiply for tiling..
}