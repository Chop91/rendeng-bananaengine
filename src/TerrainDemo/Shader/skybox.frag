#version 430 core

in vec3 f_position;

layout(location = 0) out vec4 o_color;

layout(location = 3) uniform vec3 u_camera_position;
layout(location = 4) uniform samplerCube u_texture_cube;

void main()
{
	// Cube Mapping
	vec3 I = normalize(f_position - u_camera_position);
	vec4 color = texture(u_texture_cube, I) * vec4(0.7, 0.7, 0.7, 1.0); // make it darker
	o_color = vec4(color);
}