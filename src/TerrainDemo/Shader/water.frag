#version 430 core

layout(location = 0) out vec4 o_color;

layout(location = 2) uniform sampler2D texture_diffuse;
layout(location = 3) uniform float u_alpha;

in vec2 f_uv;

void main()
{
	// Get material colors
	vec3 material_diffuse_color = texture(texture_diffuse, f_uv).rgb;

	o_color = vec4(material_diffuse_color, u_alpha);
}