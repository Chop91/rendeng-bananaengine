#version 430 core

layout(location = 1) in vec4 v_position;
layout(location = 2) in vec3 v_normal;
layout(location = 3) in vec2 v_uv;
layout(location = 4) in vec3 v_color;

out vec2 f_uv;
out vec3 f_object_normal;
out vec3 f_object_to_light_direction;

struct Light {
	vec3 light_direction;
	vec3 light_color_ambient;
	vec3 light_color_diffuse;
	vec3 light_color_specular;
	float attenuation_constant;
	float attenuation_linear;
	float attenuation_quadratic;
	float shininess;
};

layout(location = 1) uniform mat4 u_M;
layout(location = 3) uniform mat4 u_VP;
layout(location = 7) uniform Light u_light;

void main()
{
	// Positions
	gl_Position = u_VP * u_M * v_position;

	//UVs 
	f_uv = v_uv;

	// Normals
	f_object_normal = (u_M * vec4(v_normal, 0.0)).xyz;

	// Direction from the point on the surface to the light
	f_object_to_light_direction = -u_light.light_direction;
}