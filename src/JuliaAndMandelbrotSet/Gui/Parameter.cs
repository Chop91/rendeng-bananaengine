﻿using OpenTK;

namespace JuliaAndMandelbrotSet.GUI
{
    internal class Parameter
    {
        public int MaxIterations { get; set; }

        public float Scale { get; set; }

        public bool ShowMandelbrot { get; set; }

        public Vector2 C { get; set; }

        public Vector2 Center { get; set; }

        public float Fps { get; set; }
    }
}