﻿using System.Collections.Generic;
using System.Drawing;
using AntTweakBar;
using OpenTK;
using OpenTK.Input;

namespace JuliaAndMandelbrotSet.GUI
{
    internal class Gui
    {
        // Constants
        private const float MarginScale = 0.0125f;

        private readonly Context _context;
        private Bar _settingsBar;
        private Size _windowSize;
        private readonly double _renderPeriod;

        private FloatVariable _scale;
        private FloatVariable _fps;
        private StringVariable _c;

        public Parameter Parameter { get; }

        /// <summary>
        /// Constructor of the graphical user interface.
        /// </summary>
        /// <param name="windowSize">The size of the window.</param>
        /// <param name="renderPeriod">The update interval of the graphical user interface.</param>
        public Gui(Size windowSize, double renderPeriod)
        {
            _windowSize = windowSize;
            _renderPeriod = renderPeriod;

            _context = new Context(Tw.GraphicsAPI.OpenGLCore);
            Parameter = new Parameter();
            InitializeParameters();
            InitializeSettingsBar();
        }

        /// <summary>
        /// Initializes the parameter with default settings.
        /// </summary>
        private void InitializeParameters()
        {
            Parameter.MaxIterations = 100;
            Parameter.Scale = 1.0f;
            Parameter.ShowMandelbrot = true;
            Parameter.C = new Vector2(-0.6f, -0.6f);
            Parameter.Center = Vector2.Zero;
            Parameter.Fps = 0.0f;
        }

        /// <summary>
        /// Initializes the settings bar.
        /// </summary>
        private void InitializeSettingsBar()
        {
            _settingsBar = new Bar(_context)
            {
                Label = "Settings",
                Contained = true,
                Iconified = false,
                Help = "Press SPACE to hide/unhide the 'Settings'-Bar.",
                Size = new Size(350, 150),
                ValueColumnWidth = 150,
                ButtonAlignment = BarButtonAlignment.Center,
                Position =
                    new Point((int)(MarginScale * _windowSize.Width),
                        (int)(MarginScale * _windowSize.Height))
            };
            _settingsBar.SetDefinition("refresh=" + _renderPeriod + " color='17 109 143' alpha=64");

            var fractalGroup = new Group(_settingsBar);


            #region fractal

            var maxIterations = new IntVariable(_settingsBar, Parameter.MaxIterations);
            maxIterations.Label = "Max. Iterations";
            maxIterations.Help = "The maximum iterations.";
            maxIterations.SetDefinition("min=1 max=200 step=1");
            maxIterations.Changed += delegate { Parameter.MaxIterations = maxIterations.Value; };
            maxIterations.Group = fractalGroup;

            _scale = new FloatVariable(_settingsBar, Parameter.Scale);
            _scale.Label = "Scale";
            _scale.Help = "The scale of the mandelbrot set.";
            _scale.SetDefinition("min=0.1 max=10.0 step=0.1 precision=1");
            _scale.Changed += delegate { Parameter.Scale = _scale.Value; };
            _scale.Group = fractalGroup;

            var showMandelbrot = new BoolVariable(_settingsBar, Parameter.ShowMandelbrot);
            showMandelbrot.Label = "Show Mandelbrot";
            showMandelbrot.Help = "Enables/Disables the mandelbrot.";
            showMandelbrot.Changed += delegate { Parameter.ShowMandelbrot = showMandelbrot.Value; };
            showMandelbrot.Group = fractalGroup;

            var buttonResetMandelbrot = new Button(_settingsBar);
            buttonResetMandelbrot.Label = "Reset Mandelbrot";
            buttonResetMandelbrot.Help = "Resets the transformation of the mandelbrot (zoom and pan).";
            buttonResetMandelbrot.Clicked += delegate
            {
                Parameter.Scale = 1.0f;
                _scale.Value = 1.0f;
                Parameter.Center = Vector2.Zero;
            };
            buttonResetMandelbrot.Group = fractalGroup;

            _c = new StringVariable(_settingsBar, Parameter.C.ToString());
            _c.Label = "C";
            _c.Help = "Point C is selected in the complex plane in the visualization of the Mandelbrot set. C is then used as a parameter to compute the corresponding Julia set Jc.";
            _c.ReadOnly = true;
            _c.Group = fractalGroup;

            _fps = new FloatVariable(_settingsBar, Parameter.Fps);
            _fps.Label = "FPS";
            _fps.Help = "Shows the FPS.";
            _fps.SetDefinition("precision=1");
            _fps.Changed += delegate { Parameter.Fps = _fps.Value; };
            _fps.ReadOnly = true;
            _fps.Group = fractalGroup;

            #endregion


            fractalGroup.Label = "Fractal";
        }

        #region Minor event handling

        /// <summary>
        /// Updates the graphical user interface.
        /// </summary>
        public void OnUpdateFrame()
        {
            // updates some values of the gui
            _fps.Value = Parameter.Fps;
            _c.Value = Parameter.C.X.ToString("0.00000") + "+" + Parameter.C.Y.ToString("0.00000") + "i";
            _scale.Value = Parameter.Scale;
        }

        /// <summary>
        /// Renders the graphical user interface.
        /// </summary>
        public void OnRenderFrame()
        {
            _context.Draw();
        }

        public void OnResize(Size clientSize)
        {
            _context.HandleResize(clientSize);
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void OnMouseDown(MouseButtonEventArgs e)
        {
            if (HandleMouseClick(_context, e))
            {
                return;
            }
        }

        public void OnMouseUp(MouseButtonEventArgs e)
        {
            if (HandleMouseClick(_context, e))
            {
                return;
            }
        }

        public void OnMouseMove(MouseMoveEventArgs e)
        {
            if (_context.HandleMouseMove(e.Position))
            {
                return;
            }
        }

        public void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (_context.HandleMouseWheel(e.Value))
            {
                return;
            }
        }


        public void OnKeyDown(KeyboardKeyEventArgs e)
        {
            // Shortcut to iconify the settings bar
            if (e.Key.Equals(Key.Space))
            {
                _settingsBar.Iconified = !_settingsBar.Iconified;
            }

            if (HandleKeyInput(_context, e, true))
            {
                return;
            }
        }

        public void OnKeyUp(KeyboardKeyEventArgs e)
        {
            if (HandleKeyInput(_context, e, false))
            {
                return;
            }
        }

        public void OnKeyPress(KeyPressEventArgs e)
        {
            if (_context.HandleKeyPress(e.KeyChar))
            {
                return;
            }
        }

        #endregion

        // ATTENTION: COPIED FROM https://github.com/TomCrypto/AntTweakBar.NET/blob/master/Samples/OpenTK-OpenGL/Program.cs
        #region Event Conversion

        // Because OpenTK does not use an event loop, the native AntTweakBar library
        // has no provisions for directly handling user events. Therefore we need to
        // convert any OpenTK events to AntTweakBar events before handling them.

        private static bool HandleMouseClick(Context context, MouseButtonEventArgs e)
        {
            IDictionary<MouseButton, Tw.MouseButton> mapping = new Dictionary<MouseButton, Tw.MouseButton>()
            {
                { MouseButton.Left, Tw.MouseButton.Left },
                { MouseButton.Middle, Tw.MouseButton.Middle },
                { MouseButton.Right, Tw.MouseButton.Right },
            };

            var action = e.IsPressed ? Tw.MouseAction.Pressed : Tw.MouseAction.Released;

            if (mapping.ContainsKey(e.Button))
            {
                return context.HandleMouseClick(action, mapping[e.Button]);
            }
            else
            {
                return false;
            }
        }

        private static bool HandleKeyInput(Context context, KeyboardKeyEventArgs e, bool down)
        {
            IDictionary<Key, Tw.Key> mapping = new Dictionary<Key, Tw.Key>()
            {
                { Key.BackSpace, Tw.Key.Backspace },
                { Key.Tab, Tw.Key.Tab },
                { Key.Clear, Tw.Key.Clear },
                { Key.Enter, Tw.Key.Return },
                { Key.Pause, Tw.Key.Pause },
                { Key.Escape, Tw.Key.Escape },
                { Key.Space, Tw.Key.Space },
                { Key.Delete, Tw.Key.Delete },
                { Key.Up, Tw.Key.Up },
                { Key.Down, Tw.Key.Down },
                { Key.Right, Tw.Key.Right },
                { Key.Left, Tw.Key.Left },
                { Key.Insert, Tw.Key.Insert },
                { Key.Home, Tw.Key.Home },
                { Key.End, Tw.Key.End },
                { Key.PageUp, Tw.Key.PageUp },
                { Key.PageDown, Tw.Key.PageDown },
                { Key.F1, Tw.Key.F1 },
                { Key.F2, Tw.Key.F2 },
                { Key.F3, Tw.Key.F3 },
                { Key.F4, Tw.Key.F4 },
                { Key.F5, Tw.Key.F5 },
                { Key.F6, Tw.Key.F6 },
                { Key.F7, Tw.Key.F7 },
                { Key.F8, Tw.Key.F8 },
                { Key.F9, Tw.Key.F9 },
                { Key.F10, Tw.Key.F10 },
                { Key.F11, Tw.Key.F11 },
                { Key.F12, Tw.Key.F12 },
                { Key.F13, Tw.Key.F13 },
                { Key.F14, Tw.Key.F14 },
                { Key.F15, Tw.Key.F15 },
                { Key.A, Tw.Key.A },
                { Key.B, Tw.Key.B },
                { Key.C, Tw.Key.C },
                { Key.D, Tw.Key.D },
                { Key.E, Tw.Key.E },
                { Key.F, Tw.Key.F },
                { Key.G, Tw.Key.G },
                { Key.H, Tw.Key.H },
                { Key.I, Tw.Key.I },
                { Key.J, Tw.Key.J },
                { Key.K, Tw.Key.K },
                { Key.L, Tw.Key.L },
                { Key.M, Tw.Key.M },
                { Key.N, Tw.Key.N },
                { Key.O, Tw.Key.O },
                { Key.P, Tw.Key.P },
                { Key.Q, Tw.Key.Q },
                { Key.R, Tw.Key.R },
                { Key.S, Tw.Key.S },
                { Key.T, Tw.Key.T },
                { Key.U, Tw.Key.U },
                { Key.V, Tw.Key.V },
                { Key.W, Tw.Key.W },
                { Key.X, Tw.Key.X },
                { Key.Y, Tw.Key.Y },
                { Key.Z, Tw.Key.Z },
            };

            var modifiers = Tw.KeyModifiers.None;
            if (e.Modifiers.HasFlag(KeyModifiers.Alt))
                modifiers |= Tw.KeyModifiers.Alt;
            if (e.Modifiers.HasFlag(KeyModifiers.Shift))
                modifiers |= Tw.KeyModifiers.Shift;
            if (e.Modifiers.HasFlag(KeyModifiers.Control))
                modifiers |= Tw.KeyModifiers.Ctrl;

            if (mapping.ContainsKey(e.Key))
            {
                if (down)
                {
                    return context.HandleKeyDown(mapping[e.Key], modifiers);
                }
                else
                {
                    return context.HandleKeyUp(mapping[e.Key], modifiers);
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }

    internal sealed class PointVariable : StructVariable<Vector3>
    {
        private FloatVariable X => (variables[0] as FloatVariable);
        private FloatVariable Y => (variables[1] as FloatVariable);
        private FloatVariable Z => (variables[2] as FloatVariable);


        public PointVariable(Bar bar, Vector3 point)
            : base(bar, new FloatVariable(bar, point.X), new FloatVariable(bar, point.Y), new FloatVariable(bar, point.Z))
        {
            X.Label = "X";
            Y.Label = "Y";
            Z.Label = "Z";
        }

        public override Vector3 Value
        {
            get { return new Vector3(X.Value, Y.Value, Z.Value); }
            set
            {
                X.Value = value.X;
                Y.Value = value.Y;
                Z.Value = value.Z;
            }
        }

        public float Step
        {
            get { return X.Step; }
            set
            {
                X.Step = value;
                Y.Step = value;
                Z.Step = value;
            }
        }

        public float Precision
        {
            get { return X.Precision; }
            set
            {
                X.Precision = value;
                Y.Precision = value;
                Z.Precision = value;
            }
        }
    }
}