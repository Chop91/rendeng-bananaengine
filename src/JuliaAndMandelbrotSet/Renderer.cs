﻿using System;
using System.Configuration;
using System.Drawing;
using BananaEngine.Graphics;
using JuliaAndMandelbrotSet.Fractals;
using JuliaAndMandelbrotSet.GUI;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using ClearBufferMask = OpenTK.Graphics.OpenGL4.ClearBufferMask;
using EnableCap = OpenTK.Graphics.OpenGL4.EnableCap;
using GL = OpenTK.Graphics.OpenGL4.GL;

namespace JuliaAndMandelbrotSet
{
    internal class Renderer : GameWindow
    {
        private const float MinScale = 0.1f;
        private const float MaxScale = 10.0f;
        private const float ScaleStep = 0.1f;
        private readonly Vector2 _panX = new Vector2(0.1f, 0.0f);
        private readonly Vector2 _panY = new Vector2(0.0f, 0.1f);

        private bool _lockCursor;
        private JuliaSet _juliaSet;
        private MandelbrotSet _mandelbrotSet;
        private Gui _gui;

        /// <summary>
        /// Constructor of the renderer.
        /// </summary>
        public Renderer()
            : base(800, 600, new GraphicsMode(32, 24, 0, 4))
        {
            // Get data from App.config
            var windowWidth = Convert.ToInt32(ConfigurationManager.AppSettings["windowWidth"]);
            var windowHeight = Convert.ToInt32(ConfigurationManager.AppSettings["windowHeight"]);
            var fullscreen = Convert.ToBoolean(ConfigurationManager.AppSettings["fullscreen"]);
            var renderFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["renderFrequency"]);
            var updateFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["updateFrequency"]);
            var iconPath = Convert.ToString(ConfigurationManager.AppSettings["iconPath"]);
            var windowName = Convert.ToString(ConfigurationManager.AppSettings["windowName"]);

            ClientSize = new Size(windowWidth, windowHeight);
            Title = windowName;
            Icon = new Icon(iconPath);

            if (fullscreen)
            {
                WindowState = WindowState.Fullscreen;
                TargetRenderFrequency = renderFrequency;
                TargetUpdateFrequency = updateFrequency;
            }
        }

        /// <summary>
        /// Loads the scene and initializes the renderer.
        /// </summary>
        /// <param name="e">Event argument</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.ClearColor(0, 0, 0, 0);
            GL.Disable(EnableCap.DepthTest);
            WindowBorder = WindowBorder.Fixed;
            CursorVisible = true;
            _lockCursor = false;

            _gui = new Gui(ClientSize, TargetRenderPeriod);
            _juliaSet = new JuliaSet(_gui.Parameter);
            _mandelbrotSet = new MandelbrotSet(_gui.Parameter);
            TextureManager.UploadAll();
        }

        /// <summary>
        /// Updates everything.
        /// </summary>
        /// <param name="e">Frame event argument.</param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            _gui.Parameter.Fps = (float)Math.Round(RenderFrequency, 1);
            _gui.OnUpdateFrame();
        }

        /// <summary>
        /// Renders everything.
        /// </summary>
        /// <param name="e">Frame event argument.</param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Render julia set
            _juliaSet.Render();

            // Render mandelbrot set
            if (_gui.Parameter.ShowMandelbrot)
            {
                _mandelbrotSet.Render();
            }

            // Render gui
            _gui.OnRenderFrame();

            SwapBuffers();
        }

        /// <summary>
        /// Exits the demo and deletes all the opengl stuff.
        /// </summary>
        public override void Exit()
        {
            _juliaSet.Delete();
            _mandelbrotSet.Delete();
            base.Exit();
        }

        /// <summary>
        /// Gets a complex number from the mandelbrot view in the top right eighths of the screen.
        /// </summary>
        /// <param name="position">The position of the mouse</param>
        /// <returns></returns>
        private Vector2 GetC(Point position)
        {
            // Assume mandelbrot set is top right eighths of the screen
            // Calculate the min, max and delta
            var minX = ClientSize.Width * 0.75f;
            var maxX = ClientSize.Width;
            var minY = 0.0f;
            var maxY = ClientSize.Height * 0.25f;
            var deltaX = maxX - minX;
            var deltaY = maxY - minY;

            // If the mouse is outside the defined area do nothing.
            if (position.X > maxX || position.X < minX || position.Y > maxY || position.Y < minY)
            {
                return _gui.Parameter.C;
            }

            // Clamp to [min, max]
            var x = Math.Min(Math.Max(minX, position.X), maxX);
            var y = Math.Min(Math.Max(minY, position.Y), maxY);

            // Normalize mouse position
            var normalizedX = (x - minX) / deltaX;
            var normalizedY = y / deltaY;

            // Transform [0, 1] to [-2, 1]
            var transformedX = normalizedX * 3.0f - 2.0f;

            // Transform [0, 1] to [-1, 1]
            var transformedY = -(normalizedY * 2.0f - 1.0f);

            // Scale and adjust the center
            return (new Vector2(transformedX, transformedY) /_gui.Parameter.Scale) + _gui.Parameter.Center;
        }


        #region Minor event handling

        private Vector2 _lastMousePosition;

        private void ResetCursor()
        {
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
            _lastMousePosition = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            if (Focused && _lockCursor)
            {
                ResetCursor();
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            GL.Viewport(this.ClientRectangle);
            _gui.OnResize(ClientSize);
        }

        protected override void Dispose(bool manual)
        {
            _gui.Dispose();
            base.Dispose(manual);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            _gui.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            _gui.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
            _gui.OnMouseMove(e);

            if (e.Mouse.IsButtonDown(MouseButton.Left) && _gui.Parameter.ShowMandelbrot)
            {
                _gui.Parameter.C = GetC(e.Position);
            }

            if (Focused && _lockCursor)
            {
                ResetCursor();
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            _gui.OnMouseWheel(e);

            // Zoom in the mandelbrot
            var scale = _gui.Parameter.Scale + e.Delta * ScaleStep;
            if (scale >= MinScale && scale <= MaxScale)
            {
                _gui.Parameter.Scale = scale;
            }
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
            _gui.OnKeyDown(e);

            if (e.Key == Key.Escape)
            {
                Close();
            }
            // Pan in the mandelbrot
            else if (e.Key == Key.Left)
            {
                _gui.Parameter.Center -= _panX / _gui.Parameter.Scale;
            }
            else if (e.Key == Key.Right)
            {
                _gui.Parameter.Center += _panX / _gui.Parameter.Scale;
            }
            else if (e.Key == Key.Up)
            {
                _gui.Parameter.Center += _panY / _gui.Parameter.Scale;
            }
            else if (e.Key == Key.Down)
            {
                _gui.Parameter.Center -= _panY / _gui.Parameter.Scale;
            }
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            base.OnKeyUp(e);
            _gui.OnKeyUp(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            _gui.OnKeyPress(e);
        }

        #endregion
    }
}