﻿#version 430 core

in vec2 f_uv;

layout(location = 0) out vec4 o_color;

layout(location = 2) uniform sampler1D u_texture_colors;
layout(location = 3) uniform vec2 u_c;
layout(location = 4) uniform int u_max_iterations;

void main()
{
	// According to slides: fractals5 p. 27
	vec2 z;
	// Transform [0, 1] to [-1.5, 1.5]
	z.x = (f_uv.x * 2.0 - 1.0) * 1.5;
	// Transform [0, 1] to [-1, 1]
	z.y = (f_uv.y * 2.0 - 1.0) * 1.0;

	int i;
	for(i = 0; i <= u_max_iterations; i++) {
		// Multiply complex numbers and add c
		float x = (z.x * z.x - z.y * z.y) + u_c.x;
		float y = (z.y * z.x + z.x * z.y) + u_c.y;

		if((x * x + y * y) > 4.0)
		{
			break;
		}
		z.x = x;
		z.y = y;
	}

	if (i > u_max_iterations)
	{
		o_color = vec4(0.0, 0.0, 0.0, 0.0);
	}
	else
	{
		o_color = texture(u_texture_colors, float(i) / 128.0);
	}
}