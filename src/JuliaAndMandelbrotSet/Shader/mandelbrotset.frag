﻿#version 430 core

in vec2 f_uv;

layout(location = 0) out vec4 o_color;

layout(location = 2) uniform sampler1D u_texture_colors;
layout(location = 3) uniform vec2 u_center;
layout(location = 4) uniform float u_scale;
layout(location = 5) uniform int u_max_iterations;

void main()
{
	// According to slides: fractals6 p. 12
	vec2 z;
	vec2 c;
	// Transform [0, 1] to [-2, 1]
	c.x = (f_uv.x * 3.0) - 2.0;
	
	// Transform [0, 1] to [-1, 1]
	c.y = (f_uv.y * 2.0) - 1.0;

	// Apply scale
	c.x /= u_scale;
	c.y /= u_scale;

	// Apply translation of center
	c.x += (u_center.x);
	c.y += (u_center.y);
	
	int i;
	for(i = 0; i <= u_max_iterations; i++) {
		// Multiply complex numbers and add c
		float x = (z.x * z.x - z.y * z.y) + c.x;
		float y = (z.y * z.x + z.x * z.y) + c.y;

		if((x * x + y * y) > 4.0)
		{
			break;
		}
		z.x = x;
		z.y = y;
	}

	if (i > u_max_iterations)
	{
		o_color = vec4(0.0, 0.0, 0.0, 0.0);
	}
	else
	{
		o_color = texture(u_texture_colors, float(i) / 128.0);
	}
}