﻿using BananaEngine.Geometry.Models.Primitives._2D;
using BananaEngine.Graphics;
using JuliaAndMandelbrotSet.GUI;
using OpenTK.Graphics.OpenGL4;
using Matrix4 = OpenTK.Matrix4;

namespace JuliaAndMandelbrotSet.Fractals
{
    internal class MandelbrotSet
    {
        private readonly Quad2D _quad2D;
        private readonly Shader _shader;
        private readonly Parameter _parameter;
        private Matrix4 _modelMatrix;

        /// <summary>
        /// Constructor of the mandelbrot set.
        /// </summary>
        /// <param name="parameter">The parameters from the gui.</param>
        public MandelbrotSet(Parameter parameter)
        {
            _shader = ShaderManager.GetShader("./Shader/mandelbrotset.vert", "./Shader/mandelbrotset.frag");
            _quad2D = new Quad2D();
            _parameter = parameter;

            // Load color scheme texture
            const string path = @"./Data/color-scheme.png";
            _quad2D.Mesh2D.TextureDiffuse = TextureManager.GetTextureFromFilePath(path, OpenTK.Graphics.OpenGL.TextureTarget.Texture1D);

            // Calculate transformation for top right eighths of the screen
            var scale = Matrix4.CreateScale(0.25f);
            var translation = Matrix4.CreateTranslation(3.0f, 3.0f, 0.0f);
            _modelMatrix = translation * scale;
        }

        /// <summary>
        /// Renders the mandelbrot set.
        /// </summary>
        public void Render()
        {
            _shader.UseShader();

            GL.BindVertexArray(_quad2D.Mesh2D.Vao);

            // Bind matrix
            GL.UniformMatrix4(1, false, ref _modelMatrix);

            // Bind texture
            if (_quad2D.Mesh2D.TextureDiffuse.Id != null)
            {
                GL.Uniform1(2, 0);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture1D, _quad2D.Mesh2D.TextureDiffuse.Id.Value);
            }

            // Bind parameter from gui
            GL.Uniform2(3, _parameter.Center);
            GL.Uniform1(4, _parameter.Scale);
            GL.Uniform1(5, _parameter.MaxIterations);

            // Darw mesh
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.BindVertexArray(0);
        }

        /// <summary>
        /// Deletes all the opengl stuff.
        /// </summary>
        public void Delete()
        {
            _quad2D.Mesh2D.Delete();
            _shader.Unload();
        }
    }
}
