﻿using BananaEngine.Geometry.Models.Primitives._2D;
using BananaEngine.Graphics;
using JuliaAndMandelbrotSet.GUI;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace JuliaAndMandelbrotSet.Fractals
{
    internal class JuliaSet
    {
        private readonly Quad2D _quad2D;
        private readonly Shader _shader;
        private readonly Parameter _parameter;
        private Matrix4 _modelMatrix;

        /// <summary>
        /// Constructor of the julia set.
        /// </summary>
        /// <param name="parameter">The parameters from the gui.</param>
        public JuliaSet(Parameter parameter)
        {
            _shader = ShaderManager.GetShader("./Shader/juliaset.vert", "./Shader/juliaset.frag");
            _quad2D = new Quad2D();
            _parameter = parameter;

            // Load color scheme texture
            const string path = @"./Data/color-scheme.png";
            _quad2D.Mesh2D.TextureDiffuse = TextureManager.GetTextureFromFilePath(path, OpenTK.Graphics.OpenGL.TextureTarget.Texture1D);
            _modelMatrix = Matrix4.Identity;
        }

        /// <summary>
        /// Renders the julia set.
        /// </summary>
        public void Render()
        {
            _shader.UseShader();

            GL.BindVertexArray(_quad2D.Mesh2D.Vao);

            // Bind matrix
            GL.UniformMatrix4(1, false, ref _modelMatrix);

            // Bind texture
            if (_quad2D.Mesh2D.TextureDiffuse.Id != null)
            {
                GL.Uniform1(2, 0);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture1D, _quad2D.Mesh2D.TextureDiffuse.Id.Value);
            }

            // Bind parameter from gui
            GL.Uniform2(3, _parameter.C);
            GL.Uniform1(4, _parameter.MaxIterations);

            // Draw mesh
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.BindVertexArray(0);
        }

        /// <summary>
        /// Deletes all the opengl stuff.
        /// </summary>
        public void Delete()
        {
            _quad2D.Mesh2D.Delete();
            _shader.Unload();
        }
    }
}